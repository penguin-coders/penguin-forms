<?php

// Establish database connection
$cfg = require(__DIR__.'/../config/general.php');
require_once($cfg['penguin_filepath'].'/includes/testdb.php');

use PHPUnit\Framework\TestCase;

class TextElementTest extends TestCase {

	protected $el;

	protected function submitFoobar() {
	
		// Clear down any old test submisions
		global $db;
		$db->Execute("TRUNCATE form_submissions");
		$db->Execute("TRUNCATE submission_details");

		$this->el->setValue("Foobar");
		$this->el->saveToDB();
	}

	protected function setUp():void {
		// __construct($id = 0, $typeID = 0, $displayOrder = 1, $label = '', $value = '', $name = '') {
		$this->el = new TextElement(3,4,1,"Foo","Bar","foo");
	}

	public function testCreateTextElement() {
		$this->assertTrue(is_object($this->el));
	}	

/* Test public getters */

	public function testGetID() {
		$this->assertEquals(3,$this->el->getID());
	}

	public function testGetTitle() {
		$this->assertEquals("Foo",$this->el->getTitle());
	}

	public function testGetTypeID() {
		$this->assertEquals(4,$this->el->getTypeID());
	}

	public function testGetDisplayOrder() {
		$this->assertEquals(1,$this->el->getDisplayOrder());
	}
	
	public function testGetParentID() {
		$this->assertEquals(0,$this->el->getParentID());
	}

/* Test public setters */

	public function testSetDisplayOrder() {
		$this->el->setDisplayOrder(85);
		$this->assertEquals(85,$this->el->getDisplayOrder());
	}

	public function testIncrementDisplayOrder() {
		$this->el->incrementDisplayOrder();
		$this->assertEquals(2,$this->el->getDisplayOrder());
	}
	
	public function testSetParentID() {
		$this->el->setParentID(32);
		$this->assertEquals(32,$this->el->getParentID());
	}

	public function testSetValue() {
		$this->el->setValue("Foobar");
		$this->assertMatchesRegularExpression('#<input .*value=(\'Foobar\'|"Foobar").*/>#', $this->el->generateHTML());
	}

/* Test interface functions */

	public function testGenerateHTML() {
	
		$html =  $this->el->generateHTML();

		$this->assertMatchesRegularExpression('#<label>Foo.*</label>#', $html);
		$this->assertMatchesRegularExpression('#<input .*type=(\'text\'|"text").*/>#', $html);
		$this->assertMatchesRegularExpression('#<input .*name=(\'foo\'|"foo").*/>#', $html);
		$this->assertMatchesRegularExpression('#<input .*value=(\'Bar\'|"Bar").*/>#', $html);
		$this->assertMatchesRegularExpression('#<input .*id=(\'q3\'|"q3").*/>#', $html);
	}

	public function testLoadFromDB() {

		$this->el->loadFromDB();
		$html =  $this->el->generateHTML();

		$this->assertMatchesRegularExpression('#<label>First name .*</label>#', $html);
		$this->assertMatchesRegularExpression('#<input .*name=(\'First_name\'|"First_name").*/>#', $html);
		$this->assertMatchesRegularExpression('#<input .*value=(\'\'|"").*/>#', $html);
	}

	public function testSaveToDB() {
		$this->submitFoobar();
		$this->assertEquals(1,getSubmissionID());
	}

/* Test interface functions when element in edit mode */

	public function testGenerateBackendHTML() {
		
		$this->el->loadBackendFieldsets();
		
		$html = $this->el->generateBackendHTML();
		$this->assertMatchesRegularExpression('#<fieldset.*</fieldset>#', $html);
	}

	public function testEditModeGenerateHTML() {
		
		$this->el->loadBackendFieldsets();
		$this->el->setEditMode(true);

		$html = $this->el->generateHTML();
		$this->assertMatchesRegularExpression('#<fieldset.*</fieldset>#', $html);
	}


// Not working as save routine saves from $_POST not object properties
//
//	public function testEditModeSaveToDB() {
//
//		// Do the saving
//		$this->el->loadBackendFieldsets();
//		$this->el->setEditMode(true);
//		$this->el->saveToDB();
//
//		print $this->el->generateHTML();
//		// Load back in to see if it's the same
//		$this->el->setEditMode(false);
//		$this->el->loadFromDB();
//
//		$html =  $this->el->generateHTML();
//
//		$this->assertMatchesRegularExpression('#<label>Foo.*</label>#', $html);
//		$this->assertMatchesRegularExpression('#<input .*type=(\'text\'|"text").*/>#', $html);
//		$this->assertMatchesRegularExpression('#<input .*name=(\'foo\'|"foo").*/>#', $html);
//		$this->assertMatchesRegularExpression('#<input .*value=(\'Bar\'|"Bar").*/>#', $html);
//		$this->assertMatchesRegularExpression('#<input .*id=(\'q3\'|"q3").*/>#', $html);
//
//	}

}

