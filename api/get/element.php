<?php

/**************************************************************************
 * Penguin Forms                                                          *
 * Copyright (C) 2019-2021 Mark Williams <markw@posteo.net>               *
 * Copyright (C) 2019-2021 Lee Slater <lslater@posteo.net>                *
 * Copyright (C) 2019-2021 Joe Leach <joeldn@protonmail.com>              *
 *                                                                        *
 * This file is part of Penguin Forms.                                    *
 *                                                                        *
 * Penguin Forms is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by   *
 * the Free Software Foundation, either version 3 of the License, or      *
 * (at your option) any later version.                                    *
 *                                                                        *
 * Penguin Forms is distributed in the hope that it will be useful, but   *
 * WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU       *
 * General Public License for more details.                               *
 *                                                                        *
 * You should have received a copy of the GNU General Public licenses     *
 * along with Penguin Forms. If not, see <https://www.gnu.org/licenses/>. *
 **************************************************************************/

header("Content-Type: application/json");

$cfg = require(__DIR__.'/../../config/general.php');
include $cfg['penguin_filepath'].'/vendor/autoload.php';
spl_autoload_register(function ($class_name) {
	global $cfg;
	include $cfg['penguin_filepath']."/classes/".$class_name.".php";
});
require $cfg['penguin_filepath']."/includes/db.php";


if (!isset($_GET['id'])) {
	echo json_encode(["id not set"]);
}

// Lookup ID to get type
$elementLookupSQL = "select * from form_elements where id=?";
$r = $db->Execute($elementLookupSQL,[$_GET['id']]);

// Lookup class name
$elementTypeID = $r->fields['typeID'];
$elementTypeLookupSQL = "select * from form_element_types where id=?";
$r = $db->Execute($elementTypeLookupSQL,[$elementTypeID]);
$className = $r->fields['class_name'];

// New ElementType based on that lookup
$element = new $className($_GET["id"]);
$element->loadFromDB();
echo json_encode($element->generateJSON());

