-- MySQL dump 10.19  Distrib 10.3.31-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: penguin_forms
-- ------------------------------------------------------
-- Server version	10.3.31-MariaDB-0+deb10u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `element_attributes`
--

DROP TABLE IF EXISTS `element_attributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `element_attributes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parentID` int(10) unsigned NOT NULL,
  `prop_name` varchar(255) NOT NULL,
  `prop_val` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=552 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `element_attributes`
--

LOCK TABLES `element_attributes` WRITE;
/*!40000 ALTER TABLE `element_attributes` DISABLE KEYS */;
INSERT INTO `element_attributes` VALUES (1,3,'label','First name'),(2,3,'value',''),(4,4,'label','Second name'),(5,5,'name','sausage'),(6,5,'value','haggis'),(7,6,'label','Preferred text editor'),(8,7,'label','Vim'),(9,7,'value','Vim'),(10,8,'label','Nano'),(11,8,'value','Nano'),(12,9,'name','submit'),(13,9,'value','Submit'),(14,11,'label','Label'),(15,12,'label','Name'),(16,10,'label','Text box'),(17,13,'label','Value'),(18,14,'name','submit'),(19,14,'value','Save'),(20,3,'name','First_name'),(21,4,'name','Second_name'),(22,6,'name','Preferred_editor'),(23,11,'name','label'),(24,12,'name','name'),(25,13,'name','value'),(28,16,'label','New form element'),(29,17,'label','Element type'),(34,17,'name','new_element_type'),(39,22,'label','Text box'),(40,22,'value','4'),(41,4,'value',''),(42,31,'label','Email'),(43,31,'name','Email'),(44,31,'value',''),(45,1,'title','Example form 1'),(46,33,'title','Example form two'),(47,35,'label','Favourite distro'),(48,35,'name','Fave_distro'),(49,35,'value','Arch'),(50,36,'name','submit'),(51,36,'value','Submit'),(52,0,'title','New form'),(53,0,'title','New form'),(56,46,'label','Title'),(57,46,'name','new_form_title'),(58,46,'value',''),(59,47,'label','Name'),(60,47,'name','name'),(61,47,'value',''),(62,48,'label','Value'),(63,48,'name','value'),(64,48,'value',''),(65,45,'label','Submit button'),(66,50,'label','Submit button'),(67,50,'value','7'),(68,51,'name','submit'),(69,51,'value','Save'),(70,43,'label','Add new form'),(72,54,'label','Name'),(73,54,'name','name'),(74,54,'value',''),(77,56,'label','Value'),(78,56,'name','value'),(79,56,'value',''),(80,53,'label','Hidden element'),(85,59,'label','Hidden element'),(86,59,'value','3'),(87,60,'name','test'),(88,60,'value','testing'),(89,53,'title','Hidden element - editor'),(90,10,'title','TextElement editor'),(91,45,'title','Submit button - editor'),(92,14,'title','Backend submit button'),(93,16,'title','New element adder'),(94,43,'title','New form adder'),(96,62,'title','New class adder'),(97,64,'label','Class name'),(98,64,'name','new_backend_element'),(99,64,'value',''),(100,65,'name','submit'),(101,65,'value','Save'),(102,62,'label','Add new class'),(106,67,'title','Select element - editor'),(107,11,'value',''),(108,12,'value',''),(109,13,'value',''),(111,68,'label','Label'),(112,68,'name','label'),(113,68,'value',''),(114,69,'label','Name'),(115,69,'name','name'),(116,69,'value',''),(119,71,'label','Display order'),(120,71,'name','display_order'),(121,71,'value',''),(122,72,'title','Option element - editor'),(123,73,'label','Label'),(124,73,'name','label'),(125,73,'value',''),(126,74,'label','Value'),(127,74,'name','value'),(128,74,'value',''),(129,67,'label','Select element'),(130,76,'label','Display order'),(131,76,'name','display_order'),(132,72,'label','Option'),(133,75,'label','Option'),(134,77,'name','label'),(135,75,'title','Option element - draggable control'),(136,78,'title','New element adder for selects'),(137,78,'label','New form element'),(138,79,'label','Element type'),(139,79,'name','new_element_type'),(140,80,'label','Option'),(141,80,'value','6'),(142,81,'label','Emacs'),(143,81,'value','Emacs'),(144,82,'title','Form page - editor'),(145,82,'label','Form page'),(146,83,'label','Page title'),(147,88,'label','Display order'),(148,88,'name','display_order'),(149,89,'name','name'),(150,91,'label','Display order'),(151,91,'name','display_order'),(152,90,'name','label'),(153,93,'label','Display order'),(154,93,'name','display_order'),(155,92,'name','name'),(156,95,'label','Display order'),(157,95,'name','display_order'),(158,94,'name','label'),(165,83,'name','title'),(166,2,'title','An example page'),(167,102,'name','title'),(168,102,'label','Form title'),(169,101,'label','Form'),(170,105,'name','display_order'),(171,104,'name','title'),(174,106,'label','New form page'),(186,124,'title','Multi-page test'),(187,125,'title','Page 1'),(188,126,'title','Page 2'),(189,127,'label','Name'),(190,127,'name','Name'),(191,127,'value',''),(192,128,'label','What do you think of penguin forms?'),(193,128,'name','penguin_opinion'),(194,128,'value',''),(195,129,'name','submit'),(196,129,'value','Next'),(199,131,'title','Test form'),(200,132,'title','Page 1'),(201,133,'title','Page 2'),(202,134,'label','Name'),(203,134,'name','name'),(204,134,'value',''),(205,136,'label','Penguin'),(206,136,'name','penguin'),(207,136,'value','Tux'),(208,135,'name','submit'),(209,135,'value','Next'),(210,138,'name','submit'),(211,138,'value','Submit'),(212,137,'label','Test'),(213,137,'name','test'),(214,137,'value',''),(215,102,'value',''),(216,140,'label','Send submissions to email address'),(217,140,'name','submission_email'),(218,140,'value',''),(219,131,'submission_email',''),(222,1,'submission_email',''),(224,124,'submission_email',''),(226,33,'submission_email','markw@posteo.net'),(228,146,'label','Class name'),(229,146,'name','classname'),(230,146,'value','jhgjhg'),(231,139,'title','Element configurator editor'),(235,139,'label','Attributes'),(236,101,'title','Form - editor'),(237,87,'title','Select element - draggable control'),(238,87,'label',''),(239,86,'title','Submit button - draggable control'),(240,86,'label',''),(241,85,'title','Text element - draggable control'),(242,85,'label',''),(243,84,'title','Hidden element - draggable control'),(244,84,'label',''),(245,103,'title','Form page - draggable control'),(246,103,'label',''),(247,106,'title','New page adder'),(248,148,'title','Textarea - editor'),(249,148,'label',''),(250,149,'label','Label'),(251,149,'name','label'),(252,149,'value',''),(253,150,'label','Name'),(254,150,'name','name'),(255,150,'value',''),(256,151,'label','Value'),(257,151,'name','value'),(258,151,'value',''),(259,152,'title','Textarea - draggable control'),(260,152,'label',''),(262,153,'name','label'),(264,155,'label','Text area'),(265,155,'value','13'),(266,154,'label','Select element'),(267,154,'value','5'),(268,156,'label','Is it working?'),(269,156,'name','working'),(270,157,'label','Yes'),(271,157,'value','yes'),(272,158,'label','No'),(273,158,'value','no'),(274,160,'title','Legend tag - editor'),(275,161,'title','Legend Tag - draggable control'),(276,160,'label',''),(277,162,'label','Name'),(278,162,'name','name'),(279,162,'value',''),(280,163,'name','display_order'),(281,163,'value','0'),(282,159,'label','Textarea test'),(283,159,'name','textarea_test'),(284,159,'value','default txt'),(285,164,'label','ed'),(286,164,'value','ed'),(287,165,'title','Test form sep 21'),(288,165,'submission_email',''),(289,166,'title','Page one'),(290,167,'title','Page three'),(291,168,'label','Name'),(292,168,'name','name'),(293,168,'value',''),(294,169,'label','Age'),(295,169,'name','age'),(296,169,'value',''),(297,170,'label','A or B'),(298,170,'name','a_or_b'),(299,171,'label','Please select'),(300,171,'value',''),(301,173,'label','A'),(302,173,'value','a'),(303,172,'label','B'),(304,172,'value','b'),(305,174,'name','submit'),(306,174,'value','Next'),(307,175,'name','submit'),(308,175,'value','Submit'),(309,176,'name','submit'),(310,176,'value','Prev'),(311,178,'label','Question?'),(312,178,'name','question'),(313,178,'value','pre-populated value'),(318,161,'label',''),(319,177,'label','Textarea test'),(320,177,'name','textarea_test'),(321,177,'value',''),(322,181,'label','Page name'),(323,181,'name','new_page'),(324,182,'label','Form Page'),(325,182,'value','FormPage'),(329,185,'label','Conditional Form Page'),(330,185,'value','ConditionalFormPage'),(336,191,'title','Conditional page test'),(337,192,'title','Conditional form page - draggable control'),(338,193,'title','Conditional form page - editor'),(339,193,'label','Conditional form page'),(340,194,'label','Page title'),(341,194,'name','title'),(342,194,'value',''),(346,196,'name','display_order'),(347,196,'value',''),(348,198,'label','Qwerty'),(349,198,'name','qwerty'),(350,198,'value','qwerty'),(351,199,'name','submit'),(352,199,'value','Prev'),(353,200,'name','submit'),(354,200,'value','Next'),(355,201,'label','Skip page 2?'),(356,201,'name','skip_page'),(357,202,'label','Yes'),(358,202,'value','yes'),(359,203,'label','No'),(360,203,'value','no'),(364,204,'label','Condition value'),(365,204,'name','condition_value'),(366,204,'value',''),(371,191,'condition_question_id','201'),(372,191,'condition_value','no'),(373,192,'label',''),(374,197,'name','title'),(378,209,'title','Page list - editor'),(379,210,'title','Page list - draggable control'),(380,211,'label','Page List'),(381,211,'value','15'),(382,213,'name','submit'),(383,213,'value','Back'),(384,215,'name','display_order'),(385,215,'value',''),(387,219,'title','Page 3'),(388,221,'title','Optional page'),(389,221,'condition_question_id','127'),(390,221,'condition_value','Lee'),(391,223,'label','Explain'),(392,223,'name','explain'),(393,223,'value',''),(394,224,'title','PenguinForm'),(395,225,'title','FormPage'),(396,226,'title','HiddenElement'),(397,227,'title','TextElement'),(398,228,'title','SelectElement'),(399,229,'title','OptionElement'),(400,230,'title','SubmitButton'),(401,231,'title','FieldsetElement'),(402,232,'title','ListOfForms'),(403,233,'title','DraggableElementControl'),(404,234,'title','LegendTag'),(405,235,'title','ElementEditor'),(406,236,'title','TextareaElement'),(407,237,'title','ConditionalFormPage'),(408,238,'title','PageList'),(409,239,'title','ElementConfigurator'),(410,240,'title','NewElementAdder'),(418,244,'title','Editor checkboxes'),(419,244,'label','Editors'),(422,251,'title','QuestionSelector'),(423,251,'label',''),(425,257,'label','Element type'),(426,257,'name','new_element_type'),(427,258,'label','Text box'),(428,258,'value','4'),(429,259,'label','Question selector'),(430,259,'value','20'),(431,256,'title',''),(432,256,'label',''),(433,260,'label','Label'),(434,260,'name','label'),(435,260,'value',''),(436,255,'label','New element adder for ElementEditor'),(437,261,'label','Title'),(438,261,'name','title'),(439,261,'value',''),(440,255,'title','New element adder for ElementEditor'),(441,252,'title','QuestionSelector editor'),(442,252,'label',''),(443,264,'label','Label'),(444,264,'name','label'),(445,264,'value',''),(446,265,'label','Name'),(447,265,'name','name'),(448,265,'value',''),(449,235,'label',''),(450,266,'title','ElementEditor editor'),(451,267,'label','Title'),(452,267,'name','title'),(453,267,'value',''),(454,268,'label','Label'),(455,268,'name','label'),(456,268,'value',''),(457,266,'label','Attributes'),(460,269,'label','Value'),(461,269,'name','value'),(462,269,'value',''),(463,262,'label','Condition question'),(464,262,'name','condition_question_id'),(465,262,'value',''),(466,270,'title','Checkbox'),(467,270,'label',''),(468,271,'title','Checkbox editor'),(469,271,'label',''),(470,273,'label','Label'),(471,273,'name','label'),(472,273,'value',''),(473,274,'label','Name'),(474,274,'name','name'),(475,274,'value',''),(476,275,'label','Value'),(477,275,'name','value'),(478,275,'value',''),(479,276,'label','Checkbox'),(480,276,'value','21'),(481,277,'label','Checkbox'),(482,277,'value','21'),(484,34,'title',''),(485,282,'label','I agree'),(486,282,'value','agreed'),(487,282,'name','agree'),(488,283,'label','Wibble'),(489,283,'name','wibble'),(490,283,'value',''),(491,284,'label','Checked'),(492,284,'name','checked'),(493,284,'value',''),(494,285,'label','Tick me'),(495,285,'name','tickme'),(496,285,'value','tickme'),(497,285,'checked',''),(498,278,'label','Element editor'),(499,278,'name','element_editor'),(500,278,'value','12'),(501,278,'checked',''),(502,279,'label','Draggable element control'),(503,279,'name','draggable_element_control'),(504,279,'value','10'),(505,279,'checked',''),(506,280,'label','New element adder'),(507,280,'name','new_element_adder'),(508,280,'value','17'),(509,280,'checked',''),(520,313,'label','Title'),(521,313,'name','title'),(522,313,'value',''),(523,312,'title','Element configurator editor'),(524,314,'label','Label'),(525,314,'name','label'),(526,314,'value',''),(527,312,'label','Attributes'),(528,320,'title','Test'),(529,209,'label',''),(530,320,'label',''),(531,238,'label',''),(532,321,'title','ConfiguratorEditor'),(533,330,'title','AnotherTest'),(534,317,'label','New form element'),(535,317,'title','New element adder'),(536,331,'label','Element type'),(537,331,'name','new_element_type'),(538,332,'label','Text box'),(539,332,'value','4'),(540,334,'label','Checkbox'),(541,334,'value','21'),(542,333,'label','Text area'),(543,333,'value','13'),(544,335,'label','Will it save this?'),(545,335,'name','will_it_save'),(546,335,'value',''),(547,336,'title','PageGroup'),(548,340,'label','Help'),(549,340,'name','help'),(550,340,'value',''),(551,3,'help','Please enter your first name');
/*!40000 ALTER TABLE `element_attributes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form_element_types`
--

DROP TABLE IF EXISTS `form_element_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_element_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class_name` varchar(32) DEFAULT NULL,
  `configurator` int(10) unsigned DEFAULT NULL,
  `element_editor` int(10) unsigned DEFAULT NULL,
  `draggable_element_control` int(11) DEFAULT NULL,
  `new_element_adder` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form_element_types`
--

LOCK TABLES `form_element_types` WRITE;
/*!40000 ALTER TABLE `form_element_types` DISABLE KEYS */;
INSERT INTO `form_element_types` VALUES (1,'PenguinForm',224,101,NULL,106),(2,'FormPage',225,82,103,16),(3,'HiddenElement',226,53,84,NULL),(4,'TextElement',227,10,85,NULL),(5,'SelectElement',228,67,87,78),(6,'OptionElement',229,72,75,NULL),(7,'SubmitButton',230,45,86,NULL),(8,'FieldsetElement',231,NULL,NULL,NULL),(9,'ListOfForms',232,NULL,NULL,NULL),(10,'DraggableElementControl',233,312,316,NULL),(11,'LegendTag',234,160,161,NULL),(12,'ElementEditor',235,266,241,255),(13,'TextareaElement',236,148,152,NULL),(14,'ConditionalFormPage',237,193,192,317),(15,'PageList',238,209,210,NULL),(16,'ElementConfigurator',239,139,NULL,244),(17,'NewElementAdder',240,256,315,318),(20,'QuestionSelector',251,252,253,NULL),(21,'Checkbox',270,271,272,NULL),(24,'Test',320,326,327,NULL),(25,'ClassnameEditor',321,NULL,NULL,NULL),(26,'AnotherTest',330,NULL,NULL,NULL),(27,'PageGroup',336,337,338,339);
/*!40000 ALTER TABLE `form_element_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form_elements`
--

DROP TABLE IF EXISTS `form_elements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_elements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parentID` int(10) unsigned NOT NULL,
  `typeID` int(10) unsigned DEFAULT NULL,
  `display_order` int(10) unsigned DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=341 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form_elements`
--

LOCK TABLES `form_elements` WRITE;
/*!40000 ALTER TABLE `form_elements` DISABLE KEYS */;
INSERT INTO `form_elements` VALUES (1,32,1,1),(2,1,2,0),(3,2,4,0),(4,2,4,1),(5,2,3,3),(6,2,5,2),(7,6,6,0),(8,6,6,1),(9,2,7,6),(10,227,12,1),(11,10,4,0),(12,10,4,1),(13,10,4,2),(14,319,7,3),(16,225,17,2),(17,16,5,0),(22,17,6,0),(31,2,4,4),(32,0,9,1),(33,32,1,1),(34,33,2,0),(35,34,4,0),(36,34,7,1),(43,319,12,1),(45,230,12,1),(46,43,4,0),(47,45,4,0),(48,45,4,1),(50,17,6,4),(51,43,7,1),(53,226,12,1),(54,53,4,0),(56,53,4,1),(59,17,6,5),(60,2,3,5),(61,0,9,1),(62,319,12,1),(64,62,4,0),(65,62,7,1),(67,228,12,1),(68,67,4,0),(69,67,4,1),(71,0,4,2),(72,229,12,1),(73,72,4,0),(74,72,4,1),(75,229,10,1),(76,75,3,0),(77,75,11,0),(78,228,17,2),(79,78,5,0),(80,79,6,1),(81,6,6,2),(82,225,12,1),(83,82,4,0),(84,226,10,0),(85,227,10,0),(86,230,10,0),(87,228,10,0),(88,84,3,0),(89,84,11,0),(90,85,11,0),(91,85,3,1),(92,86,11,0),(93,86,3,1),(94,87,11,0),(95,87,3,1),(101,224,12,0),(102,101,4,0),(103,225,10,1),(104,103,11,0),(105,103,3,1),(106,224,17,2),(124,32,1,1),(125,124,2,0),(126,124,2,2),(127,125,4,1),(128,126,4,1),(129,126,7,3),(131,32,1,1),(132,131,2,0),(133,131,2,1),(134,132,4,0),(135,132,7,2),(136,132,4,1),(137,133,4,0),(138,133,7,2),(139,239,12,0),(140,101,4,1),(146,139,25,0),(148,236,12,1),(149,148,4,0),(150,148,4,1),(151,148,4,2),(152,236,10,1),(153,152,11,1),(154,17,6,3),(155,17,6,2),(156,133,5,1),(157,156,6,0),(158,156,6,0),(159,133,13,1),(160,234,12,1),(161,234,10,1),(162,160,4,0),(163,160,3,1),(164,6,6,0),(165,32,1,1),(166,165,2,0),(167,165,2,2),(168,166,4,0),(169,166,4,1),(170,167,5,0),(171,170,6,0),(172,170,6,1),(173,170,6,0),(174,166,7,3),(175,167,7,4),(176,167,7,3),(177,167,13,1),(178,167,4,2),(181,106,5,0),(182,181,6,0),(185,181,6,0),(191,165,14,1),(192,237,10,1),(193,237,12,1),(194,193,4,0),(196,192,3,1),(197,192,11,1),(198,191,4,0),(199,191,7,1),(200,191,7,2),(201,166,5,2),(202,201,6,0),(203,201,6,0),(204,193,4,2),(209,238,12,1),(210,238,10,1),(211,17,6,6),(212,125,15,0),(213,126,7,2),(214,212,3,1),(215,210,3,1),(216,210,11,0),(218,126,15,0),(219,124,2,3),(220,219,15,0),(221,124,14,1),(222,221,15,0),(223,221,13,0),(224,61,16,0),(225,61,16,0),(226,61,16,0),(227,61,16,0),(228,61,16,0),(229,61,16,0),(230,61,16,0),(231,61,16,0),(232,61,16,0),(233,319,16,0),(234,319,16,0),(235,319,16,0),(236,61,16,0),(237,61,16,0),(238,61,16,0),(239,319,16,0),(240,319,16,0),(241,235,10,0),(242,241,11,0),(243,241,3,1),(244,239,17,2),(251,319,16,1),(252,251,12,0),(253,251,10,0),(255,235,17,2),(256,240,12,0),(257,255,5,0),(258,257,6,0),(259,257,6,2),(260,256,4,0),(261,256,4,1),(262,193,20,0),(264,252,4,0),(265,252,4,1),(266,235,12,0),(267,266,4,0),(268,266,4,1),(269,252,4,2),(270,61,16,1),(271,270,12,0),(272,270,10,0),(273,271,4,0),(274,271,4,1),(275,271,4,2),(276,257,6,1),(277,17,6,1),(278,244,21,0),(279,244,21,0),(280,244,21,0),(282,34,21,0),(283,132,21,0),(284,271,4,3),(285,166,21,0),(312,233,12,0),(313,312,4,0),(314,312,4,1),(315,240,10,0),(316,233,10,0),(317,237,17,2),(318,240,17,2),(319,0,9,1),(320,61,16,1),(321,61,16,1),(322,316,11,0),(323,316,3,1),(326,320,12,0),(327,320,10,0),(328,161,3,0),(329,161,11,0),(330,61,16,1),(331,317,5,0),(332,331,6,0),(333,331,6,1),(334,331,6,0),(335,191,13,0),(336,61,16,1),(337,336,12,0),(338,336,10,0),(339,336,17,0),(340,10,4,3);
/*!40000 ALTER TABLE `form_elements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form_submissions`
--

DROP TABLE IF EXISTS `form_submissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_submissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `formID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form_submissions`
--

LOCK TABLES `form_submissions` WRITE;
/*!40000 ALTER TABLE `form_submissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `form_submissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `penguin_forms_admin`
--

DROP TABLE IF EXISTS `penguin_forms_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `penguin_forms_admin` (
  `userID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(60) NOT NULL,
  `lastLoginDate` datetime DEFAULT NULL,
  `passwordExpires` datetime NOT NULL,
  `live` tinyint(4) NOT NULL DEFAULT 0,
  `loginCount` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `penguin_forms_admin`
--

LOCK TABLES `penguin_forms_admin` WRITE;
/*!40000 ALTER TABLE `penguin_forms_admin` DISABLE KEYS */;
/*!40000 ALTER TABLE `penguin_forms_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `submission_details`
--

DROP TABLE IF EXISTS `submission_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `submission_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `submissionID` int(10) unsigned NOT NULL,
  `questionID` int(10) unsigned NOT NULL,
  `answer` varchar(255) NOT NULL,
  `tainted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `submission_details`
--

LOCK TABLES `submission_details` WRITE;
/*!40000 ALTER TABLE `submission_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `submission_details` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-12 10:39:54
