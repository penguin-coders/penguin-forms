<?php

/**************************************************************************
 * Penguin Forms                                                          *
 * Copyright (C) 2019-2021 Mark Williams <markw@posteo.net>               *
 * Copyright (C) 2019-2021 Lee Slater <lslater@posteo.net>                *
 * Copyright (C) 2019-2021 Joe Leach <joeldn@protonmail.com>              *
 *                                                                        *
 * This file is part of Penguin Forms.                                    *
 *                                                                        *
 * Penguin Forms is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by   *
 * the Free Software Foundation, either version 3 of the License, or      *
 * (at your option) any later version.                                    *
 *                                                                        *
 * Penguin Forms is distributed in the hope that it will be useful, but   *
 * WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU       *
 * General Public License for more details.                               *
 *                                                                        *
 * You should have received a copy of the GNU General Public licenses     *
 * along with Penguin Forms. If not, see <https://www.gnu.org/licenses/>. *
 **************************************************************************/

require_once(__DIR__."/includes/top.php");

// Check to see if we have been passed a form ID
// If not, redirect to index.php
if(isset($_GET['elementID'])) {
	$elementID = intval($_GET['elementID']);
} else {
	header('Location:'.$cfg['penguin_root'].'/admin/index.php');
}

?>

<script>
	function jquery_ui() {
		$( "#draggableFieldsets>div" ).sortable({
			connectWith: "div"
		});
		$( "#draggableFieldsets>div" ).droppable();
		$( "#draggableFieldsets>div" ).disableSelection();
	}

	var ordered = 0;

	$(function() {
		jquery_ui();

		document.querySelector("form > input[name='submit']").onclick = (function(e) {
			if (ordered==0) {
				e.preventDefault();
				set_order();
				ordered = 1;
			}
			document.querySelector("form >input[name='submit']").click();
		})
	});

	function set_order() {
		var children = document.querySelectorAll("form fieldset.ui-sortable-handle:not(.ui-sortable-placeholder)");

		for (var i=0; i<children.length; i++) {
			var question = children[i];
			if (question.type=="fieldset") {
				var components = question.children;
				for (var j = 0; j < components.length; j++) {
					if (components[j].tagName=="INPUT") {
						if (components[j].name.substr(0,13) == "display_order") {
							components[j].value = i;
						}
					}
				}

			}
		}
	}
</script>

<?php

$subquery = "SELECT parentID FROM form_elements WHERE id=".intval($_GET['elementID']);
$r = $db->Execute("select id,typeID from form_elements where id=(".$subquery.")");
if ($r->fields['typeID']=="9") {
	echo "<a class='up-element' href='".$cfg['penguin_root']."/admin/listforms.php?listID=".$r->fields['id']."'>Back to forms list</a>";
	echo "<a id='view-live' target='_blank' href='".$cfg['penguin_root']."/penguin.php?formID=".intval($_GET['elementID'])."'>View form</a>";
} else {
	echo "<a class='up-element' href='".$cfg['penguin_root']."/admin/edit.php?elementID=".$r->fields['id']."'>Back to parent element</a>";
}

$class_name = getElementType($elementID);
$formID = ($class_name=='PenguinForm') ? $elementID : getFormID($elementID);

// Load the element to be edited, then attach to a dummy form
$elem = new $class_name($elementID);
$elem->setFormID($formID);
$elem->loadFromDB();
$elem->loadEditors();
$elem->setEditMode(); // Ensures backend HTML will be returned for this element
$penguin = new BackendForm(0);
$penguin->attachElement($elem);

// Attach a submit button
$penguin->addSubmit();

+d($elem);
d($_POST);

// Save the form if just submitted
if(isset($_POST['submit'])) {
	$penguin->saveToDB();
	print "<div class='tmp-notification'><span>Form submitted</span></div>";
	$penguin->reloadFromDB();
}

print "<h1>Element:".$elem->getTitle()."</h1>"; // TODO - make this into a breadcrumb
print $penguin->generateHTML();

require_once(__DIR__."/includes/tail.php");
?>

