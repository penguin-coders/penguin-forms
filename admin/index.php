<?php

/**************************************************************************
 * Penguin Forms                                                          *
 * Copyright (C) 2019-2021 Mark Williams <markw@posteo.net>               *
 * Copyright (C) 2019-2021 Lee Slater <lslater@posteo.net>                *
 * Copyright (C) 2019-2021 Joe Leach <joeldn@protonmail.com>              *
 *                                                                        *
 * This file is part of Penguin Forms.                                    *
 *                                                                        *
 * Penguin Forms is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by   *
 * the Free Software Foundation, either version 3 of the License, or      *
 * (at your option) any later version.                                    *
 *                                                                        *
 * Penguin Forms is distributed in the hope that it will be useful, but   *
 * WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU       *
 * General Public License for more details.                               *
 *                                                                        *
 * You should have received a copy of the GNU General Public licenses     *
 * along with Penguin Forms. If not, see <https://www.gnu.org/licenses/>. *
 **************************************************************************/

require_once(__DIR__."/includes/top.php");

if(isset($_POST['submit'])) {
	$_SESSION['gitlog'] = ($_POST['gitlog']=='on');
	$_SESSION['debug'] = ($_POST['debug']=='on');
	$_SESSION['testdb'] = ($_POST['testdb']=='on');
}

if(isset($_SESSION['gitlog'])) {
	$gitlog = $_SESSION['gitlog'];
} else {
	$gitlog = false;
	$_SESSION['gitlog'] = $gitlog;
}

if(isset($_SESSION['debug'])) {
	$debug = $_SESSION['debug'];
} else {
	$debug = false;
	$_SESSION['debug'] = $debug;
}

if(isset($_SESSION['testdb'])) {
	$testdb = $_SESSION['testdb'];
} else {
	$testdb = false;
	$_SESSION['testdb'] = $testdb;
}

?>
<h1>Penguin forms admin</h1>
<div>
<p><b>Session variables</b></p>
<form method='post'>
	<input type='checkbox' name='debug' <?php if($debug) { print "checked='checked'"; } ?> id='debug' /><label for='debug'>Enable kint debugging</label><br />
	<input type='checkbox' name='gitlog' <?php if($gitlog) { print "checked='checked'"; } ?> id='gitlog' /><label for='gitlog'>Show git-log output below</label><br />
	<input type='checkbox' name='testdb' <?php if($testdb) { print "checked='checked'"; } ?> id='testdb' /><label for='testdb'>Use test database <span style='color:#f00;'>(<b>Warning:</b> requires manual set up.)</span></label><br />
	<input type='submit' name='submit' value='Save' />
</form>
</div>
<?php 
	if($gitlog) { 
?>
<div>
<h2>Recent changes</h2>
<p>Here are the most recent changes from the git log:</p>
<code>
<pre>
<?php print htmlspecialchars(shell_exec("git log -n 5"));?>
</pre>
</code>
</div>
<?php
	}

require_once(__DIR__."/includes/tail.php");
