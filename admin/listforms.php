<?php

/**************************************************************************
 * Penguin Forms                                                          *
 * Copyright (C) 2019-2021 Mark Williams <markw@posteo.net>               *
 * Copyright (C) 2019-2021 Lee Slater <lslater@posteo.net>                *
 * Copyright (C) 2019-2021 Joe Leach <joeldn@protonmail.com>              *
 *                                                                        *
 * This file is part of Penguin Forms.                                    *
 *                                                                        *
 * Penguin Forms is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by   *
 * the Free Software Foundation, either version 3 of the License, or      *
 * (at your option) any later version.                                    *
 *                                                                        *
 * Penguin Forms is distributed in the hope that it will be useful, but   *
 * WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU       *
 * General Public License for more details.                               *
 *                                                                        *
 * You should have received a copy of the GNU General Public licenses     *
 * along with Penguin Forms. If not, see <https://www.gnu.org/licenses/>. *
 **************************************************************************/

require_once(__DIR__."/includes/top.php");

// Get list ID
$listID = intval($_GET['listID']);

// Determine corresponding fieldsetID
switch($listID) {
	case 32: // front-end elements
		$fieldsetID = 43;
		$msg="<h1>Form builder</h1>"; 
		break;
	case 61: 
		$fieldsetID = 62; 
		$msg="<h1>Element builder</h1>";
		$msg .= "<p>Here you can create or edit the form elements which are used to build penguin forms.</p>";
		$msg .= "<p><b>Please note : </b>Changes made here will update the settings for the elements in the database only. You'll probably need to follow up with some PHP coding to fully implement your changes.</p>";
		$msg .= "<p><b style='color:#f00;'>Warning : </b>It's possible to break stuff here. We assume you know what you're doing.</p>";	
		break;
	case 319:
		$fieldsetID = 0;
		$msg="<h1>Edit backend elements</h1>";
		$msg .= "<p>Here you can edit a few special elements used within the form builder and the element builder.</p>";
		$msg .= "<p><b>Please note : </b>Changes made here will update the settings for the elements in the database only. You'll probably need to follow up with some PHP coding to fully implement your changes.</p>";
		$msg .= "<p><b style='color:#f00;'>Warning : </b>It's possible to break stuff here. We assume you know what you're doing.</p>";	
}

if($fieldsetID > 0) {
	// Load 'add new form' fieldset
	$addForm = new FieldsetElement($fieldsetID);
	$addForm->loadFromDB();
	$addForm->setFrontendElementIDsOfChildElements($listID);

	// Check if 'add new form' submitted
	if(isset($_POST['submit'])) {
		$addForm->saveToDB();
		print "<p>Form added.</p>";
	}
}

// Load form list - doing this post-save means new form will be included
$list = new ListOfFormsBackend($listID);
$list->setFormID(0);
$list->loadFromDB();

// Kint debugging
+d($list);

// Output HTML
print $msg;
print $list->generateHTML();
if($fieldsetID>0) {
	print "<form method='post'>";
	print $addForm->generateHTML();
	print "</form>";
}

require_once(__DIR__."/includes/tail.php");
