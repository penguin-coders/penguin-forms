<?php
header('Content-type: text/css');

if (isset($_COOKIE["theme"]) && $_COOKIE["theme"]=="dark") {
	echo "@import 'dark.css';";
} else {
	echo "@import 'light.css';";
}
echo "@import 'penguinStyle.css';";

?>
