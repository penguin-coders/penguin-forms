<?php

/**************************************************************************
 * Penguin Forms                                                          *
 * Copyright (C) 2019-2021 Mark Williams <markw@posteo.net>               *
 * Copyright (C) 2019-2021 Lee Slater <lslater@posteo.net>                *
 * Copyright (C) 2019-2021 Joe Leach <joeldn@protonmail.com>              *
 *                                                                        *
 * This file is part of Penguin Forms.                                    *
 *                                                                        *
 * Penguin Forms is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by   *
 * the Free Software Foundation, either version 3 of the License, or      *
 * (at your option) any later version.                                    *
 *                                                                        *
 * Penguin Forms is distributed in the hope that it will be useful, but   *
 * WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU       *
 * General Public License for more details.                               *
 *                                                                        *
 * You should have received a copy of the GNU General Public licenses     *
 * along with Penguin Forms. If not, see <https://www.gnu.org/licenses/>. *
 **************************************************************************/

session_start();

$cfg = require(__DIR__.'/../../config/general.php');

// Check database etc has been set up. If not, redirect to setup.php
if (!file_exists(__DIR__."/../../config/database.php") || !file_exists(__DIR__."/../../vendor")) {
	header('Location:'.$cfg['penguin_root'].'/setup.php');
}

// Redirect to login page if applicable
if(is_numeric(strpos($_SERVER['REQUEST_URI'],'/admin/')) && !isset($_SESSION['penguinAdminID'])) {
	header('Location:'.$cfg['penguin_root'].'/login/login.php');
}

include $cfg['penguin_filepath'].'/vendor/autoload.php';
if(!isset($_SESSION['debug']) || !$_SESSION['debug']) {
	Kint::$enabled_mode = false;
}

$cfg = require(__DIR__.'/../../config/general.php');

require_once($cfg['penguin_filepath'].'/includes/db.php');

spl_autoload_register(function($class) {
	global $cfg;	
	include_once($cfg['penguin_filepath']."/classes/".$class.".php");
});

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Penguin forms</title>
<link rel='stylesheet' href='<?php print $cfg['penguin_root']; ?>/admin/includes/css/theme.php' />
<?php
$jQueryConfig = require_once($cfg['penguin_filepath']."/config/jquery.php");
print "<script src='".$jQueryConfig['jQueryLocation']."'></script>";
print "<script src='".$jQueryConfig['jQueryUILocation']."'></script>";
?>
</head>

<?php
if(is_numeric(strpos($_SERVER['REQUEST_URI'],'/admin/')) && isset($_SESSION['penguinAdminID'])) {
?>
<body class="admin">
<header>
	<nav>
		<ul>
			<li><a href='<?php print $cfg['penguin_root']; ?>/admin/'>Admin home</a></li>
			<li><a href='<?php print $cfg['penguin_root']; ?>/admin/listforms.php?listID=32'>Form builder</a></li>
			<li><a href='<?php print $cfg['penguin_root']; ?>/admin/listforms.php?listID=61'>Element builder</a></li>
			<li><a href='<?php print $cfg['penguin_root']; ?>/admin/listforms.php?listID=319'>Edit backend elements</a></li>
			<li><a href='<?php print $cfg['penguin_root']; ?>/' target='_blank'>View front-end</a></li>
			<li><a href='<?php print $cfg['penguin_root']; ?>/admin/submissions.php'>View submissions</a></li>
			<li><a href='<?php print $cfg['penguin_root']; ?>/admin/change-theme.php'>Change theme</a></li>
			<li><a href='<?php print $cfg['penguin_root']; ?>/login/changepassword.php'>Change password</a></li>
			<li><a href='<?php print $cfg['penguin_root']; ?>/login/logout.php'>Logout</a></li>
		</ul>
	</nav>
</header>

<?php
} else {
?>
<body>
<?php
}
?>
<main>
<div>

