<?php

/**************************************************************************
 * Penguin Forms                                                          *
 * Copyright (C) 2019-2021 Mark Williams <markw@posteo.net>               *
 * Copyright (C) 2019-2021 Lee Slater <lslater@posteo.net>                *
 * Copyright (C) 2019-2021 Joe Leach <joeldn@protonmail.com>              *
 *                                                                        *
 * This file is part of Penguin Forms.                                    *
 *                                                                        *
 * Penguin Forms is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by   *
 * the Free Software Foundation, either version 3 of the License, or      *
 * (at your option) any later version.                                    *
 *                                                                        *
 * Penguin Forms is distributed in the hope that it will be useful, but   *
 * WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU       *
 * General Public License for more details.                               *
 *                                                                        *
 * You should have received a copy of the GNU General Public licenses     *
 * along with Penguin Forms. If not, see <https://www.gnu.org/licenses/>. *
 **************************************************************************/

require_once(__DIR__."/includes/top.php");

// Check to see if we have been passed a submission ID
// If not, redirect to index.php
if(isset($_GET['submissionID'])) {
	$submissionID = intval($_GET['submissionID']);
	$sql = "SELECT * FROM form_submissions WHERE id=".$submissionID;

	$r=$db->Execute($sql);
	$formID = $r->fields['formID'];

	$penguin = new PenguinForm($formID);
	$penguin->loadFromDB();

	print $penguin->generateAnswersTable($submissionID);
} else {
//	header('Location:'.$cfg['penguin_root'].'/admin/index.php');
	
	$sql = "SELECT * FROM form_submissions";
	$r=$db->Execute($sql);

	if($r->RecordCount()==0) {
		print "<p>No submissions.</p>";
	} else {
		print "<p><ul>\n";
		for ($i=0; $i<$r->RecordCount(); $i++) {
			$submissionID = $r->fields['id'];
			$url = $cfg['penguin_root']."/admin/submissions.php?submissionID=".$submissionID;
			print "<li><a href='".$url."'>Submission ID ".$submissionID."</a></li>";
			$r->MoveNext();
		}
		print "</ul></p>";
	}
}

require_once(__DIR__."/includes/tail.php");
?>

