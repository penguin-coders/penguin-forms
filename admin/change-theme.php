<?php
require_once(__DIR__."/includes/top.php");

if (isset($_POST["theme"])) {
	setcookie("theme", $_POST["theme"], time() + (86400 * 365), "/"); // 86400 = 1 day
	$theme = $_POST["theme"];
} elseif (isset($_COOKIE["theme"])) {
	$theme = $_COOKIE["theme"];
} else {
	$theme = "light";
}

$selected_dark = "";
$selected_light = " selected";
if ($theme=="dark") {
	$selected_dark = " selected";
	$selected_light = "";
}

?>

<form method="post">
	<fieldset>
		<legend>Filter</legend>
		<label for="filter">Status</label>
		<select type="text" name="theme" value="" id="filter">
			<option value="light"<?php echo $selected_light; ?>>Light (Default)</option>
			<option value="dark"<?php echo $selected_dark; ?>>Dark</option>
		</select>
		<input type="submit" name="submit" value="Search"><br>
	</fieldset>
</form>

