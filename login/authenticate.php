<?php

/**************************************************************************
 * Penguin Forms                                                          *
 * Copyright (C) 2019-2021 Mark Williams <markw@posteo.net>               *
 * Copyright (C) 2019-2021 Lee Slater <lslater@posteo.net>                *
 * Copyright (C) 2019-2021 Joe Leach <joeldn@protonmail.com>              *
 *                                                                        *
 * This file is part of Penguin Forms.                                    *
 *                                                                        *
 * Penguin Forms is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by   *
 * the Free Software Foundation, either version 3 of the License, or      *
 * (at your option) any later version.                                    *
 *                                                                        *
 * Penguin Forms is distributed in the hope that it will be useful, but   *
 * WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU       *
 * General Public License for more details.                               *
 *                                                                        *
 * You should have received a copy of the GNU General Public licenses     *
 * along with Penguin Forms. If not, see <https://www.gnu.org/licenses/>. *
 **************************************************************************/

$cfg = require_once(__DIR__.'/../config/general.php');
include($cfg['penguin_filepath'].'/vendor/autoload.php');

require_once($cfg['penguin_filepath'].'/includes/db.php');
require_once($cfg['penguin_filepath'].'/login/classes/adminUser.php');

if (isset($_SESSION['expiredPasswordUserID'])) {
	$adminUser = getAdminUser($_SESSION['expiredPasswordUserID']);
} elseif (isset($_SESSION['changePasswordUserID'])) {
	$adminUser = getAdminUser($_SESSION['changePasswordUserID']);
} else {
	$adminUser = getAdminUserFromEmail($_REQUEST['email']);
}

if ($adminUser->live==0 && $adminUser->userID!=-1) {
	$_SESSION['incorrect'] = true;
	$redirect = $cfg['penguin_root']."/login/login.php";
} else {
	
	require $cfg['penguin_filepath'].'/thirdparty/PasswordHash.php';
	$hasher = new PasswordHash(8, FALSE);
	$check = $hasher->CheckPassword($_REQUEST['password'], $adminUser->password);

	if ($check) {
	
		if (isset($_SESSION['expiredPasswordUserID']) || isset($_SESSION['changePasswordUserID'])) {
		
			if ($_REQUEST['newpassword']==$_REQUEST['newpassword2']) {

				#re-set password in database
				$password = $_REQUEST['newpassword'];
				$adminUser->password = $hasher->HashPassword($password);
				
				$date = new DateTime('now');
				$adminUser->lastLoginDate = $date->format('Y-m-d');
				
				$date->add(new DateInterval('P90D'));
				$adminUser->passwordExpires =  $date->format('Y-m-d');
				
				$adminUser->loginCount++;
				updateAdminUser($adminUser);
				// auditLogin($adminUser);
				
				#swap session vars
				unset($_SESSION['expiredPasswordUserID']);
				unset($_SESSION['changePasswordUserID']);
				$_SESSION['penguinAdminID'] = $adminUser->userID;
				
				$redirect = $cfg['penguin_root']."/login/login.php";

			} else {
				$_SESSION['mismatch'] = true;
				$redirect = $cfg['penguin_root']."/login/changepassword.php";
			}
			
		} else {
			
			$date1 = new DateTime("now");
			$date2 = new DateTime($adminUser->passwordExpires);
			$diff = $date1->diff($date2);
			$days = $diff->format("%r%a")-0;

			if ($days <= 0) {
				$_SESSION['expiredPasswordUserID'] = $adminUser->userID;
				$redirect = $cfg['penguin_root']."/login/changepassword.php";
			} else {
				#
				# User logged in 
				# Set session variable and update loginCount and lastLoginDate
				#
				
				$_SESSION['penguinAdminID'] = $adminUser->userID;
				
				$adminUser->lastLoginDate = $date1->format('Y-m-d');
				$adminUser->loginCount++;
				updateAdminUser ($adminUser);
				// auditLogin($adminUser);
				
				$redirect = $cfg['penguin_root']."/admin/index.php";
				
			}
		}
	} else {

		#
		# Password incorrect
		#	

		$_SESSION['incorrect'] = true;
		$redirect = $cfg['penguin_root']."/login/login.php";
		
// debug info
// print $_REQUEST['email'] . '<br />';
// print $_REQUEST['password'] . '<br />';
// print $adminUser->email . '<br />';
// print $adminUser->password . '<br />';

	}
//}
// print_r($adminUser);
// print_r ($_SESSION);
// if(isset($days)) print $days;
//	print "<a href='$redirect'>continue</a>";
header("Location:$redirect");

	}

?>
