<?php

/**************************************************************************
 * Penguin Forms                                                          *
 * Copyright (C) 2019-2021 Mark Williams <markw@posteo.net>               *
 * Copyright (C) 2019-2021 Lee Slater <lslater@posteo.net>                *
 * Copyright (C) 2019-2021 Joe Leach <joeldn@protonmail.com>              *
 *                                                                        *
 * This file is part of Penguin Forms.                                    *
 *                                                                        *
 * Penguin Forms is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by   *
 * the Free Software Foundation, either version 3 of the License, or      *
 * (at your option) any later version.                                    *
 *                                                                        *
 * Penguin Forms is distributed in the hope that it will be useful, but   *
 * WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU       *
 * General Public License for more details.                               *
 *                                                                        *
 * You should have received a copy of the GNU General Public licenses     *
 * along with Penguin Forms. If not, see <https://www.gnu.org/licenses/>. *
 **************************************************************************/

require_once('../includes/top.php');
	
	$errors = array();

	function generatePassword()	{

		$length=9;
	
		$vowels = 'aeuy';
		$consonants = 'bdghjmnpqrstvz';
		$consonants .= 'BDGHJLMNPQRSTVWXZ';
		$vowels .= "AEUY";
		$consonants .= '23456789';
		$consonants .= '@#$%';
	
		$password = '';
		$alt = time() % 2;
		for ($i = 0; $i < $length; $i++) {
			if ($alt == 1) {
				$password .= $consonants[(rand() % strlen($consonants))];
				$alt = 0;
			} else {
				$password .= $vowels[(rand() % strlen($vowels))];
				$alt = 1;
			}
		}
		return $password;
	}

	
	if(isset($_REQUEST['submit'])) {

		$required_fields = array("email");
		foreach ($required_fields as $field) {
			if (empty($_REQUEST[$field])) {
				$errors[$field] = true;
			}
		}
		
		include_once('classes/adminUser.php');

		$adminUser = getAdminUserFromEmail($_REQUEST['email']);
		
		if ($adminUser->email != $_REQUEST['email']) {
			$errors['email'] = true;
		} elseif(count($errors)==0) {

			require '../thirdparty/PasswordHash.php';
			$hasher = new PasswordHash(8, FALSE);
			
			$password = generatePassword();
			$adminUser->password = $hasher->HashPassword($password);
			
			$date = new DateTime('now');
			$adminUser->passwordExpires =  $date->format('Y-m-d');
			
			updateAdminUser($adminUser);
			
			// email admin
			
			$sender="markw@posteo.net";
			$recipient = $adminUser->email;
			
			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
			$headers .= 'From: '.$sender . "\r\n";

			$subject= "Penguin Forms Admin Password";

			$message = "<p>Your password has been reset. Please log in with the following details:</p>";
			$message .= "<p><strong>Email: </strong>" . $adminUser->email . "<br />";	
			$message .= "<p><strong>Password: </strong>" . $password . "<br />";
			$message .= "<p><strong>This is a temporary password - you'll need to re-set it as soon as you log in.</strong></p>";
print $message;
//			mail($recipient, $subject, $message, $headers);	
					
		}
	}

	function warningHtml($field, $label) {
		global $errors;
		if (isset($errors[$field])) {
			return "<strong>! ". $label . "</strong>";
		} else {
			return $label;
		}
	}

?>	
<h1>Penguin forms admin</h1>
<?php	
	if(!isset($_REQUEST['submit']) || count($errors)>0) {

?>
<h2>Password Reset</h2>
<div>
<?php
	if(count($errors)!=0) {
?>	
		<p style="color:#f00;"><strong>Please check your email address and try again.</strong></p>
<?php
	}
?>
<p>Please enter the email address you use to log in, and a new password will be e-mailed to you.</p>
<form action="<?php print $cfg['penguin_root']; ?>/login/passwordreset.php" method="post">

    <label for="email"><?php print warningHtml("email", "Email"); ?> <em>(required)</em></label>
    <input type="text"  name="email" id="email" value="" />
    
	<input type="submit" value="Reset Password" name="submit" />
    
</form>
</div>
<?php
	} else {
?>
    <h2>Password Reset</h2>
    <div>
    <p>A new password has been sent to your email address.</p>
	 <p><a href="<?php print $cfg['penguin_root']; ?>/login/login.php">Log in</a></p>
    </p>
<?php
	}

require_once('../includes/tail.php');
?>   
