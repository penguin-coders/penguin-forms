<?php

/**************************************************************************
 * Penguin Forms                                                          *
 * Copyright (C) 2019-2021 Mark Williams <markw@posteo.net>               *
 * Copyright (C) 2019-2021 Lee Slater <lslater@posteo.net>                *
 * Copyright (C) 2019-2021 Joe Leach <joeldn@protonmail.com>              *
 *                                                                        *
 * This file is part of Penguin Forms.                                    *
 *                                                                        *
 * Penguin Forms is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by   *
 * the Free Software Foundation, either version 3 of the License, or      *
 * (at your option) any later version.                                    *
 *                                                                        *
 * Penguin Forms is distributed in the hope that it will be useful, but   *
 * WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU       *
 * General Public License for more details.                               *
 *                                                                        *
 * You should have received a copy of the GNU General Public licenses     *
 * along with Penguin Forms. If not, see <https://www.gnu.org/licenses/>. *
 **************************************************************************/

require_once('../includes/top.php');

	if(isset($_SESSION['expiredPasswordUserID'])) {
		$messages = "<p>Your password has expired, please choose a new one.</p>";
		$h2 = "Expired password";
	} else {
		$messages = "";
		$h2 = "Change password";
		$_SESSION['changePasswordUserID'] = $_SESSION['penguinAdminID'];
	}
	if (isset($_SESSION['incorrect'])) {
		$messages .= "<p>The old password you entered was incorrect. Please try again.</p>";
		unset($_SESSION['incorrect']);
	}
	if (isset($_SESSION['mismatch'])) {
		$messages .= "<p>The new passwords do not match. Please try again.</p>";
		unset($_SESSION['mismatch']);
	}
	
?>
    <h1>Penguin forms admin</h1>
	 <h2><?php print $h2; ?></h2>
    <div>
    <?php print $messages; ?>
	<form action="<?php print $cfg['penguin_root']; ?>/login/authenticate.php" method="post">
        <label for="password">Old Password</label><br />
        <input type="password" value="" name="password" id="password" /><br />
        <label for="newpassword">New Password</label><br />
        <input type="password" value="" name="newpassword" id="newpassword" /><br />
        <label for="newpassword2">Confirm New Password</label><br />
        <input type="password" value="" name="newpassword2" id="newpassword2" /><br />
        <input type="submit" value="Change Password" name="submit" />
    </form>
    </div>
<?php
require_once('../includes/tail.php');
