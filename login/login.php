<?php

/**************************************************************************
 * Penguin Forms                                                          *
 * Copyright (C) 2019-2021 Mark Williams <markw@posteo.net>               *
 * Copyright (C) 2019-2021 Lee Slater <lslater@posteo.net>                *
 * Copyright (C) 2019-2021 Joe Leach <joeldn@protonmail.com>              *
 *                                                                        *
 * This file is part of Penguin Forms.                                    *
 *                                                                        *
 * Penguin Forms is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by   *
 * the Free Software Foundation, either version 3 of the License, or      *
 * (at your option) any later version.                                    *
 *                                                                        *
 * Penguin Forms is distributed in the hope that it will be useful, but   *
 * WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU       *
 * General Public License for more details.                               *
 *                                                                        *
 * You should have received a copy of the GNU General Public licenses     *
 * along with Penguin Forms. If not, see <https://www.gnu.org/licenses/>. *
 **************************************************************************/

require_once('../includes/top.php');

$OFFLINE = false;
#$OFFLINE = true;
if(isset($_GET['online']) || isset($_SESSION['online'])) {
	$OFFLINE = false;
	$_SESSION['online']=true;
}

$messages = "";

if (isset($_SESSION['incorrect'])) {
	$messages .= "<p>Incorrect username or password. Please try again.</p>";
	unset($_SESSION['incorrect']);
}

if(isset($_SESSION['logout'])) {
	unset($_SESSION['logout']);
	$messages .= "<p>You are now logged out.</p>";
}

?>
	<h1>Penguin forms admin</h1>
    <h2>Login</h2>
    <div>
<?php 
	if(!$OFFLINE) {
		print $messages; 
?>

	<form action="<?php print $cfg['penguin_root']; ?>/login/authenticate.php" method="post">
        <label for="email">Email</label><br />
        <input type="text" value="" name="email" id="email" /><br />
        <label for="password">Password</label><br />
        <input type="password" value="" name="password" id="password" /><br />
        <input type="submit" value="Log In" name="submit" />
	 </form>
</div>
<div>
    <ul>
		<li><a href="<?php print $cfg['penguin_root']; ?>/login/passwordreset.php">Reset your password</a></li>
    </ul>
<?php
	} else {
		print "<p />The admin pages are currently offline for scheduled maintenance.";
	}
?>
    </div>

<?php
require_once('../includes/tail.php');
	
