<?php

/**************************************************************************
 * Penguin Forms                                                          *
 * Copyright (C) 2019-2021 Mark Williams <markw@posteo.net>               *
 * Copyright (C) 2019-2021 Lee Slater <lslater@posteo.net>                *
 * Copyright (C) 2019-2021 Joe Leach <joeldn@protonmail.com>              *
 *                                                                        *
 * This file is part of Penguin Forms.                                    *
 *                                                                        *
 * Penguin Forms is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by   *
 * the Free Software Foundation, either version 3 of the License, or      *
 * (at your option) any later version.                                    *
 *                                                                        *
 * Penguin Forms is distributed in the hope that it will be useful, but   *
 * WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU       *
 * General Public License for more details.                               *
 *                                                                        *
 * You should have received a copy of the GNU General Public licenses     *
 * along with Penguin Forms. If not, see <https://www.gnu.org/licenses/>. *
 **************************************************************************/

class adminUser {

	public $userID;
	public $firstname;
	public $lastname;
	public $email;
	public $password;
	public $lastLoginDate;
	public $passwordExpires;
	public $live;
	public $loginCount;
	
	function __construct ()
	{
		$this->userID = -1;
		$this->firstname = '';
		$this->lastname = '';
		$this->email = '';
		$this->password = '';
		$this->lastLoginDate = '';
		$this->passwordExpires = '';
		$this->live = 0;
		$this->loginCount = 0;
	}
	
}

function newAdminUser ($adminUser)
{
	global $db;
	
	$query = 'INSERT INTO penguin_forms_admin (firstname, lastname, email, password, lastLoginDate, passwordExpires, live) '.
		'VALUES '.
		'('.
		$db->qstr($adminUser->firstname).', '.
		$db->qstr($adminUser->lastname).', '.
		$db->qstr($adminUser->email).', '.
		$db->qstr($adminUser->password).', '.
		$db->qstr($adminUser->lastLoginDate).', '.
		$db->qstr($adminUser->passwordExpires).', '.
		intval($adminUser->live).
		')';
	
	$db->Execute($query);
	
	if ($db->ErrorMsg) {
		print $db->ErrorMsg().'<br />';
	}
	
	$id = $db->Insert_ID();

	return $id;
}


function getAdminUser ($id)
{
	global $db;
	
	$query = 'SELECT * FROM penguin_forms_admin '.
		'WHERE userID='.intval($id);
		
	$result = $db->Execute($query);
	
	$adminUser = new AdminUser();
	
	$adminUser->userID = $result->fields['userID'];
	$adminUser->firstname = $result->fields['firstname'];
	$adminUser->lastname = $result->fields['lastname'];
	$adminUser->email = $result->fields['email'];
	$adminUser->password = $result->fields['password'];
	$adminUser->lastLoginDate = $result->fields['lastLoginDate'];
	$adminUser->passwordExpires = $result->fields['passwordExpires'];
	$adminUser->live = $result->fields['live'];
	$adminUser->loginCount = $result->fields['loginCount'];
	
	return $adminUser;
}

function deleteAdminUser ($id)
{
	global $db;
	
	$query = 'DELETE FROM penguin_forms_admin '.
		'WHERE userID='.intval($id);
		
	$result = $db->Execute($query);
	
}

function getAdminUserFromEmail ($email)
{
	global $db;
	
	$query = 'SELECT * FROM penguin_forms_admin '.
		'WHERE email='.$db->qstr($email);
	//print $query;
	$result = $db->Execute($query);
	
	$adminUser = new AdminUser();
	
	if(!$result->EOF) {
		
		$adminUser->userID = $result->fields['userID'];
		$adminUser->firstname = $result->fields['firstname'];
		$adminUser->lastname = $result->fields['lastname'];
		$adminUser->email = $result->fields['email'];
		$adminUser->password = $result->fields['password'];
		$adminUser->lastLoginDate = $result->fields['lastLoginDate'];
		$adminUser->passwordExpires = $result->fields['passwordExpires'];
		$adminUser->live = $result->fields['live'];
		$adminUser->loginCount = $result->fields['loginCount'];
		
	}

	return $adminUser;
}


function updateAdminUser ($adminUser)
{
	global $db;
	
	$query = 'UPDATE penguin_forms_admin '.
		'SET firstname='.$db->qstr($adminUser->firstname).', '.
		'lastname='.$db->qstr($adminUser->lastname).', '.
		'email='.$db->qstr($adminUser->email).', '.
		'password='.$db->qstr($adminUser->password).', '.
		'lastLoginDate='.$db->qstr($adminUser->lastLoginDate).', '.
		'passwordExpires='.$db->qstr($adminUser->passwordExpires).', '.
		'live='.intval($adminUser->live).', '.
		'loginCount='.intval($adminUser->loginCount).' '.
		'WHERE userID='.intval($adminUser->userID);

		error_log("update admin user: ".$query);

	
	$db->Execute($query);
	
}

