# Penguin Forms

Forms for Penguins

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

#### A LAMP stack 

This will also need Git and PHP Composer installed; a docker container might be the easiest start

Clone this repo under the web root

Then from within the project directory, invoke Composer to install `adodb` for PHP database abstraction 

```sh
composer install
```

#### A database with a name 

You'll need this later to configure the application, the setup script used in the next step looks for `penguin_forms` if no other database name is given

Note: If you are using mysql, this can be created from the setup script

```sh
mysql
create database penguin_forms;
```

### Installing 

just run `php setup.php` and follow the prompts, this creates `config/database.php` and `config/jquery.php`

```sh
/www/penguin-forms # php setup.php
Note - once this process has been completed you will be able to edit the resulting configuration in the files within the 'config' directory

Please enter the database driver (Default='mysqli'):
No driver defined, defaulting to 'mysqli'

Please enter the database address (Default='127.0.0.1'):
No host defined, defaulting to '127.0.0.1'

WARNING - if the database name is that of an existing database, it will be modified with the required tables and fields for penguin forms to operate. This may overwrite existing tables if they are named the same.

Please enter the database name (Default='penguin_forms'):
No database name defined, defaulting to 'penguin_forms'

Please enter the database username (Default='root'):
No username defined, defaulting to 'root'

Please enter the database password (Default='Password'):
No password defined, defaulting to 'Password'

Your configuration files have been saved in '/www/penguin-forms/config/'
You can cancel the process here, and when you return the configuration will pick up where you left off

Do you wish to run the database setup now? [Y/n]: Y

Setting up database...

Database setup complete

Do you wish to setup jQuery now? [Y/n]: Y
Please enter the location for jQuery (Default='https://code.jquery.com/jquery-1.12.4.js'): https://code.jquery.com/jquery-1.12.4.js

Please enter the location for jQueryUI (Default='https://code.jquery.com/ui/1.12.1/jquery-ui.js'): https://code.jquery.com/ui/1.12.1/jquery-ui.js
```

Once that's done you can browse `penguin-forms` url, fill in some forms...

...and then have a look at the database to see if you submissions have been saved:

```sql
MariaDB [penguin_forms]> select prop_val, answer 
from submission_details inner join element_attributes 
on questionID = parentID 
where prop_name = "label";
+------------------+----------+
| prop_val         | answer   |
+------------------+----------+
| First name       | foo      |
| Last name        | bar      |
| Preferred editor | emacs    |
| Email            | foo@bar  |
+------------------+----------+
```

## Licence

This project is licensed under the GPL v3 License - see the [LICENSE](LICENSE) file for details

