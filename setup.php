<?php

/**************************************************************************
 * Penguin Forms                                                          *
 * Copyright (C) 2019-2021 Mark Williams <markw@posteo.net>               *
 * Copyright (C) 2019-2021 Lee Slater <lslater@posteo.net>                *
 * Copyright (C) 2019-2021 Joe Leach <joeldn@protonmail.com>              *
 *                                                                        *
 * This file is part of Penguin Forms.                                    *
 *                                                                        *
 * Penguin Forms is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by   *
 * the Free Software Foundation, either version 3 of the License, or      *
 * (at your option) any later version.                                    *
 *                                                                        *
 * Penguin Forms is distributed in the hope that it will be useful, but   *
 * WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU       *
 * General Public License for more details.                               *
 *                                                                        *
 * You should have received a copy of the GNU General Public licenses     *
 * along with Penguin Forms. If not, see <https://www.gnu.org/licenses/>. *
 **************************************************************************/

/**
 * Installation script to setup configuration
 * TODO: Figure out a way to create the database
 */

if (php_sapi_name()==="cli") {

	$EOL = "\n";

	// Check if directory is readable, exit with error if not
	if (!is_writable(__DIR__)) {
		echo "This directory was detected as not being writable. Please ensure permissions are set correctly. Exiting.\n\n";
		exit;
	}
	if (!file_exists(dirname(__FILE__."/vendor"))) {
		echo "The vendor folder was not detected, before running this script please install dependencies";
	}


	echo "Note - once this process has been completed you will be able".
		" to edit the resulting configuration in the files within the 'config' directory\n\n";

	// General config
	if (file_exists(__DIR__."/config/general.php")) {
		echo "Existing general configuration has been detected in config/general.php\n";
		echo "Would you like to use the existing configuration? [Y/n]: ";
		$handle = fopen ("php://stdin","r");
		$answer = trim(fgets($handle));
		if (strtolower($answer)=='y' || strtolower($answer)=='yes' || $answer=='') {
			$configureGeneral = false;
		} else {
			$configureGeneral = true;
		}
		fclose($handle);
	} else {
		$configureGeneral = true;
	}
	if ($configureGeneral==true) {
		echo "Please enter the web server root (default='/var/www/html') : ";
		$handle = fopen ("php://stdin","r");
		$web_root = trim(fgets($handle));
		if (empty($web_root)) $web_root = "/var/www/html";
		if (substr($web_root,-1)=="/") $web_root = substr($web_root,0,-1);
		fclose($handle);

		$penguin_filepath = __DIR__;
		$penguin_root = str_replace($web_root,"",$penguin_filepath);

		echo "\n";
		generate_config("general.php",[
			'web_root' => $web_root,
			'penguin_filepath' => $penguin_filepath,
			'penguin_root' => $penguin_root,
			'debug' => false
		]);
	}

	// Database config
	if (file_exists(__DIR__."/config/database.php")) {
		echo "Existing database configuration has been detected in config/database.php\n";
		echo "Would you like to use the existing configuration to build the database? [Y/n]: ";
		$handle = fopen ("php://stdin","r");
		$answer = trim(fgets($handle));
		if (strtolower($answer)=='y' || strtolower($answer)=='yes' || $answer=='') {
			$configureDB = false;
		} else {
			$configureDB = true;
		}
		fclose($handle);
	} else {
		$configureDB = true;
	}
	if ($configureDB) {
		echo "Please enter the database driver (Default='mysqli'): ";
		$handle = fopen ("php://stdin","r");
		$driver = trim(fgets($handle));
		if ($driver == '') {
			echo "No driver defined, defaulting to 'mysqli'\n";
			$driver = "mysqli";
		}
		fclose($handle);
		echo "\n";

		echo "Please enter the database address (Default='127.0.0.1'): ";
		$handle = fopen ("php://stdin","r");
		$address = trim(fgets($handle));
		if ($address == '') {
			echo "No database address defined, defaulting to '127.0.0.1'\n";
			$address = "127.0.0.1";
		}
		fclose($handle);
		echo "\n";


		echo "WARNING - if the database name is that of an existing database, it".
				" will be modified with the required tables and fields for penguin forms to operate. ".
				"This may overwrite existing tables if they are named the same. \n\n";


		echo "Please enter the database name (Default='penguin_forms'): ";
		$handle = fopen ("php://stdin","r");
		$database = trim(fgets($handle));
		if ($database == '') {
			echo "No database name defined, defaulting to 'penguin_forms'\n";
			$database = "penguin_forms";
		}
		fclose($handle);
		echo "\n";


		echo "Please enter the database username (Default='root'): ";
		$handle = fopen ("php://stdin","r");
		$username = trim(fgets($handle));
		if ($username == '') {
			echo "No username defined, defaulting to 'root'\n";
			$username = "root";
		}
		fclose($handle);
		echo "\n";


		echo "Please enter the database password (Default='Password'): ";
		$handle = fopen ("php://stdin","r");
		$password = trim(fgets($handle));
		if ($password == '') {
			echo "No password defined, defaulting to 'Password'\n";
			$password = "Password";
		}
		fclose($handle);
		echo "\n";

		// setup_database_config($driver,$address,$database,$username,$password);
		generate_config("database.php",[
			'driver'   => $driver,
			'address'  => $address,
			'database' => $database,
			'username' => $username,
			'password' => $password,
			'debug'    => false
		]);

		echo "Your configuration files have been saved in '".__DIR__."/config/'\n".
				"You can cancel the process here, and when you return the configuration will pick up where you left off\n\n";

	}

	// Database setup (table creation, etc)
	echo "Do you wish to run the database setup now? [Y/n]: ";
	$handle = fopen ("php://stdin","r");
	$confirm = trim(fgets($handle));
	fclose($handle);
		// Attempt to setup the database with those details
		echo "\n";

		require_once(__DIR__."/vendor/adodb/adodb-php/adodb.inc.php");
		$dbConfig = require(__DIR__."/config/database.php");
		switch($dbConfig['driver']) {
			case "mysqli":
				$conn = new mysqli($dbConfig['address'], $dbConfig['username'], $dbConfig['password']);
				if ($conn->connect_error) {
				    die("Connection failed: " . $conn->connect_error);
				}
				$sql = "CREATE DATABASE ".$dbConfig['database'];
				if ($conn->query($sql) === TRUE) {
					echo "Database created successfully\n\n";
				} else {
					echo "Error creating database: ".$conn->error."\n\n";
				}
				$conn->close();
				break;
		}
		$db = ADONewConnection($dbConfig['driver']);
		$db->debug = false;
		$db->Connect($dbConfig['address'], $dbConfig['username'], $dbConfig['password'], $dbConfig['database']);

	if (strtolower($confirm) == 'y' || strtolower($confirm) == 'yes' || $confirm == '') {
		setup_database();
	}

	echo "Do you wish to setup an admin user now? [Y/n]: ";
	$handle = fopen ("php://stdin","r");
	$confirm = trim(fgets($handle));
	fclose($handle);
	if (strtolower($confirm) == 'y' || strtolower($confirm) == 'yes' || $confirm == '') {

		echo "Please enter the admin user's first name : ";
		$handle = fopen ("php://stdin","r");
		$adminFirstName = trim(fgets($handle));
		fclose($handle);
		echo "\n";

		echo "Please enter the admin user's last name : ";
		$handle = fopen ("php://stdin","r");
		$adminLastName = trim(fgets($handle));
		fclose($handle);
		echo "\n";

		echo "Please enter an email address for the admin user : ";
		$handle = fopen ("php://stdin","r");
		$adminEmail = trim(fgets($handle));
		fclose($handle);
		echo "\n";

		echo "Please enter a password for the admin user : ";
		$handle = fopen ("php://stdin","r");
		$adminPass = trim(fgets($handle));
		fclose($handle);
		echo "\n";

		if($adminEmail == "" || $adminPass == "") {
			echo "Unable to create admin user: email or password not specified.\n";
		} else {
			create_admin_user($adminFirstName, $adminLastName, $adminEmail, $adminPass);
		}

	}

	echo "Do you wish to setup jQuery now? [Y/n]: ";
	$handle = fopen ("php://stdin","r");
	$confirm = trim(fgets($handle));
	fclose($handle);
	if (strtolower($confirm) == 'y' || strtolower($confirm) == 'yes' || $confirm == '') {

		echo "Please enter the location for jQuery (Default='https://code.jquery.com/jquery-1.12.4.js'): ";
		$handle = fopen ("php://stdin","r");
		$jQuery = trim(fgets($handle));
		if ($jQuery == '') {
			echo "No location defined, defaulting to remote\n";
			$jQuery = "https://code.jquery.com/jquery-1.12.4.js";
		}
		fclose($handle);
		echo "\n";

		echo "Please enter the location for jQueryUI (Default='https://code.jquery.com/ui/1.12.1/jquery-ui.js'): ";
		$handle = fopen ("php://stdin","r");
		$jQueryUI = trim(fgets($handle));
		if ($jQueryUI == '') {
			echo "No location defined, defaulting to remote\n";
			$jQueryUI = "https://code.jquery.com/ui/1.12.1/jquery-ui.js";
		}
		fclose($handle);
		echo "\n";

		// setup_jquery($jQuery,$jQueryUI);
		generate_config("jquery.php",[
			'jQueryLocation' => $jQuery,
			'jQueryUILocation' => $jQueryUI
		]);
	}






} else {

	$EOL = "<br>";

	if (isset($_POST['database'])) {
		// Results of setup

		// setup_database_config($_POST['driver'],$_POST['address'],$_POST['database'],$_POST['username'],$_POST['password']);
		generate_config("database.php",[
			'driver'   => $_POST['driver'],
			'address'  => $_POST['address'],
			'database' => $_POST['database'],
			'username' => $_POST['username'],
			'password' => $_POST['password'],
			'debug'    => false
		]);

		require_once(__DIR__."/vendor/adodb/adodb-php/adodb.inc.php");
		$dbConfig = require(__DIR__."/config/database.php");
		$db = ADONewConnection($dbConfig['driver']);
		$db->debug = false;
		$db->Connect($dbConfig['address'], $dbConfig['username'], $dbConfig['password'], $dbConfig['database']);
		
		setup_database();

		generate_config("jquery.php",[
			'jQueryLocation' => $_POST['jQuery'],
			'jQueryUILocation' => $_POST['jQueryUI']
		]);

		generate_config("general.php",[
			'web_root' => $_SERVER['DOCUMENT_ROOT'],
			'penguin_filepath' => __DIR__,
			'penguin_root' => str_replace('/setup.php','',$_SERVER['REQUEST_URI']),
			'debug' => false
		]);

		create_admin_user($_POST['adminFirstName'], $_POST['adminLastName'], $_POST['adminEmail'], $_POST['adminPassword']);

	} else {
		// Initial screen
		
		echo "<h1>Penguin forms setup</h1>";
		
		// Check if directory is readable, exit with error if not
		if (!is_writable(__DIR__)) {
			echo "<p>This directory was detected as not being writable by the web user.</p>";
			echo "<p>Please ensure permissions are set correctly.</p>";
			exit;
		}
		if (!file_exists(dirname(__FILE__."/vendor"))) {
			echo "<p>The vendor folder was not detected, before running this script please install dependencies</p>";
			exit;
		}

		?>
		<form method="POST">
			<!-- TODO: Add id and for attributes to this form -->
			<!-- TODO: Add in password confirmation fields -->
			<!-- TODO: Pre-populate with any existing setting if partial setup already done -->
			<label>Driver</label>
			<input name="driver" type="text" value="mysqli" required>
			<br>
			<label>Address</label>
			<input name="address" type="text" value="127.0.0.1" required>
			<br>
			<label>Database</label>
			<input name="database" type="text" value="penguin_forms" required>
			<br>
			<label>Username</label>
			<input name="username" type="text" value="root" required>
			<br>
			<label>Password (default: Password123)</label>
			<input name="password" type="password" placeholder="Password123" required>
			<br>
			<label>jQuery script location</label>
			<input name="jQuery" type="text" value="https://code.jquery.com/jquery-1.12.4.js" required>
			<br>
			<label>jQuery UI script location</label>
			<input name="jQueryUI" type="text" value="https://code.jquery.com/ui/1.12.1/jquery-ui.js" required>
			<br>
			<label>Admin user first name</label>
			<input name="adminFirstName" type="text" required>
			<br>
			<label>Admin user last name</label>
			<input name="adminLastName" type="text" required>
			<br>
			<label>Admin user email</label>
			<input name="adminEmail" type="text" required>
			<br>
			<label>Admin user password</label>
			<input name="adminPassword" type="password" required>
			<br>
			<input name="submit" type="submit">
		</form>
		<?php
	}
}


/**
 * Generic config file creation
 * $config_file: The name of the config file to be generated
 * $settings: An associative array of settings to go within the file
 */
function generate_config($config_file, $settings) {
	// Write details to config/*.php
	if (!file_exists(__DIR__."/config")) {
		mkdir(__DIR__."/config");
	}
	if (file_exists(__DIR__."/config/".$config_file)) {
		unlink(__DIR__."/config/".$config_file);
	}

	// TODO: Support for subdirectories to be created if defined in $config_file
	
	$file = fopen(__DIR__."/config/".$config_file, "w") or die("Unable to open config file, please ensure directory permissions are correct");
	fwrite($file,"<?php\n\n");
	fwrite($file,"return [\n");
	$count = 0;
	foreach ($settings as $setting => $value) {
		$count++;
		if ($count==count($settings)) {
			fwrite($file,"\t'".$setting."' => '".$value."'\n");
		} else {
			fwrite($file,"\t'".$setting."' => '".$value."',\n");
		}
	}
	fwrite($file,"];\n\n");
}


/**
 * Read config/database.php and setup the database
 */
function setup_database() {

	global $EOL, $db;

	echo "Setting up database...".$EOL.$EOL;


	if ($db->_errorMsg!="") {
		echo $db->_errorMsg.$EOL.$EOL;
		echo "Error detected, please fix the issues listed then re-run the script.\n";
		exit;
	}

	// Read through the SQL dump in ./db, filter out the comments and run each command one-by-one
	$handle = fopen(__DIR__."/db/penguin_forms.sql", "r");
	$sql = "";
	while (($buffer = fgets($handle, 4069)) !== false) {
		if (strpos($buffer,"--")===false && strpos($buffer,"/*")===false) {
			$sql.= $buffer;
		}
	}
	if (!feof($handle)) {
		echo "Error: unexpected fgets() fail".$EOL.$EOL;
	}
	fclose($handle);
	$queries = explode(";",$sql);
	foreach ($queries as $query) {
		if ($query=="") continue;
		$r = $db->Execute($query);
		// TODO: Check if error occurs
	}

	echo "Database setup complete".$EOL.$EOL;

}


function create_admin_user($adminFirstName, $adminLastName, $adminEmail, $adminPass) {

	global $db;

   require __DIR__.'/thirdparty/PasswordHash.php';
   $hasher = new PasswordHash(8, FALSE);
  	$hashedPass = $hasher->HashPassword($adminPass);

	$dt = new DateTime();
	$dt->add(new DateInterval("P90D"));
	$expiryDate = $dt->format("Y-m-d H:i:s");

	$sql = "INSERT INTO `penguin_forms_admin` (`firstname`,`lastname`,`email`,`password`,"
	  	."`lastLoginDate`,`passwordExpires`,`live`,`loginCount`) "
		."VALUES (".$db->qstr($adminFirstName).",".$db->qstr($adminLastName).",".$db->qstr($adminEmail).","
		.$db->qstr($hashedPass).",".$db->qstr(date('Y-m-d h:i:s',0)).",".$db->qstr($expiryDate).",1,4)";

	$db->Execute($sql);
}
