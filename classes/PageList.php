<?php

/**************************************************************************
 * Penguin Forms                                                          *
 * Copyright (C) 2019-2021 Mark Williams <markw@posteo.net>               *
 * Copyright (C) 2019-2021 Lee Slater <lslater@posteo.net>                *
 * Copyright (C) 2019-2021 Joe Leach <joeldn@protonmail.com>              *
 *                                                                        *
 * This file is part of Penguin Forms.                                    *
 *                                                                        *
 * Penguin Forms is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by   *
 * the Free Software Foundation, either version 3 of the License, or      *
 * (at your option) any later version.                                    *
 *                                                                        *
 * Penguin Forms is distributed in the hope that it will be useful, but   *
 * WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU       *
 * General Public License for more details.                               *
 *                                                                        *
 * You should have received a copy of the GNU General Public licenses     *
 * along with Penguin Forms. If not, see <https://www.gnu.org/licenses/>. *
 **************************************************************************/

require_once(__DIR__."/../interfaces/PenguinFormInterface.php");
require_once(__DIR__."/abstract/SimpleFormElement.php");

class PageList extends SimpleFormElement implements PenguinFormInterface {

	public function generateHTML() {
		global $db;
		global $_SESSION;
		if($this->editMode) {
		  $html = $this->editor->generateHTML();	
		} else {
			$pages_query = "select * from form_elements where parentID=?";
			$pages_r = $db->Execute($pages_query,[$_GET['formID']]);
			$id = 'q'.$this->id;
			$nameAttribute = $this->getNameAttribute();
			$html = "<ul name='".$nameAttribute."' id='".$id."' class='page-list'>";
			if ($_SESSION['currentPage']==0) {
				$html .= "<li><input name='submit' type='submit' value='Back' disabled/></li>";
			} else {
				$html .= "<li><input name='submit' type='submit' value='Back'/></li>";
			}
			for ($i=0; $i<$pages_r->RecordCount(); $i++) {
				$page = $i + 1;
				if ($page==($_SESSION['currentPage']+1)) {
					$html .= "<li class='active'><input type='submit' value='$page' disabled/></li>";
				} else {
					$html .= "<li><input name='submit' type='submit' value='$page'/></li>";
				}
				$pages_r->MoveNext();
			}
			if ($_SESSION['currentPage']==($pages_r->RecordCount()-1)) {
				$html .= "<li><input name='submit' type='submit' value='Submit' /></li>";
			} else {
				$html .= "<li><input name='submit' type='submit' value='Next'/></li>";
			}
			$html .= "</ul>";
		}
		return $html;
	}

	public function saveToDB() {
		// Not implemented
	}

}
