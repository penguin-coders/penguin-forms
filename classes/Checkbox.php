<?php

/**************************************************************************
 * Penguin Forms                                                          *
 * Copyright (C) 2019-2021 Mark Williams <markw@posteo.net>               *
 * Copyright (C) 2019-2021 Lee Slater <lslater@posteo.net>                *
 * Copyright (C) 2019-2021 Joe Leach <joeldn@protonmail.com>              *
 *                                                                        *
 * This file is part of Penguin Forms.                                    *
 *                                                                        *
 * Penguin Forms is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by   *
 * the Free Software Foundation, either version 3 of the License, or      *
 * (at your option) any later version.                                    *
 *                                                                        *
 * Penguin Forms is distributed in the hope that it will be useful, but   *
 * WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU       *
 * General Public License for more details.                               *
 *                                                                        *
 * You should have received a copy of the GNU General Public licenses     *
 * along with Penguin Forms. If not, see <https://www.gnu.org/licenses/>. *
 **************************************************************************/

require_once(__DIR__."/../interfaces/PenguinFormInterface.php");
require_once(__DIR__."/abstract/SimpleFormElement.php");
		
class Checkbox extends SimpleFormElement implements PenguinFormInterface {

	protected $checked;
	protected $saveValue;
	
	public function generateHTML() {
		if($this->editMode) {
		  $html = $this->editor->generateHTML();	
		} else {
			//$html = "<p>Output HTML has not been specified for element 'Checkbox'. Please implement a generateHTML method.</p>";
			$id = 'q'.$this->id;
			$name = $this->getNameAttribute();
			$html = "<div><input type='checkbox' $this->checked name='$name' value='$this->value' id='$id'><label class='inline' for='$id'>$this->label</label></div>";
		}
		return $html;
	}

	public function loadFromDB() {
		parent::loadFromDB();
		if($this->checked != '') {
			$this->checked = "checked='checked'";
		}
	}

	public function setChecked($checked) {
		if($checked) {
			$this->checked = "checked='checked'";
		} else {
			$this->checked = "";
		}
	}

	public function saveToDB() {
		parent::saveToDB();
	}
	
	protected function loadSubmittedValue($submissionID) {

		global $db;

		$sql = "SELECT `answer` FROM `submission_details` WHERE questionID=".intval($this->id)." AND submissionID=".intval($submissionID)." ORDER BY id DESC";
		$r = $db->Execute($sql);
		if($r->RecordCount()>0) {
			$this->checked = ($r->fields['answer']==$this->value) ? "checked='checked'" : "";
		}
	}

	protected function saveSubmissionDetails() {

		global $db, $_POST;

		if(isset($_POST[$this->name])) {
			$answer = $_POST[$this->name];
		} else {
			$answer = ""; 
		}
	
		$submissionID = getSubmissionID();
		$submissionDetailsID = $this->getSubmissionDetailsID($submissionID);

		if($submissionDetailsID>0) {
			$sql = "UPDATE `submission_details` SET answer=".$db->qstr($answer).", tainted=0 WHERE id=".intval($submissionDetailsID);
		} else {
			$sql = "INSERT INTO `submission_details` (submissionID,questionID,answer,tainted) "
				."VALUES (".$submissionID.",".$this->id.",".$db->qstr($answer).",0)";
		}

		$db->Execute($sql);

	}
	
	public function saveElementAttributes() {
		global $db, $_POST;
		if(isset($_POST[$this->name][$this->frontendElementID])) {

			$answer = $_POST[$this->name][$this->frontendElementID];

			switch($this->name) {
				case 'element_editor':
				case 'draggable_element_control':
				case 'new_element_adder':
					if($answer != "") {
						
						// Check element not already recorded in DB
						$typeID = $this->getEditorTypeID($this->name);
						$sql = "SELECT id from form_elements WHERE parentID=$this->frontendElementID AND typeID=$typeID";
						$r = $db->Execute($sql);
						
						if($r->RecordCount()==0) {
							$sql = "INSERT INTO `form_elements` (parentID,typeID,display_order) VALUES ($this->frontendElementID,".intval($answer).",$this->display_order)";
						}	
					}
					break;
				case 'display_order':
					$sql = "UPDATE `form_elements` SET `display_order`=".intval($answer)." "
						."WHERE `id`=".intval($this->frontendElementID);
					break;
				default:

					$sql = "SELECT `id` FROM `element_attributes` "
						."WHERE `parentID`=".intval($this->frontendElementID)." "
						."AND `prop_name`=".$db->qstr($this->name);

					$r = $db->Execute($sql);

					if($r->RecordCount() > 0) {
						$sql = "UPDATE `element_attributes` SET `prop_val`=".$db->qstr($answer)." "
							."WHERE `id`=".intval($r->fields['id']);
					} else {
						$sql = "INSERT INTO `element_attributes` (parentID,prop_name,prop_val) "
							."VALUES (".intval($this->frontendElementID)
							.",".$db->qstr($this->name)
							.",".$db->qstr($answer).")";
					}
			}

		} else {

			$typeID = $this->getEditorTypeID($this->name);

			$sql = "SELECT `id` FROM `form_elements` "
				."WHERE `parentID`=".intval($this->frontendElementID)." "
				."AND `typeID`=".$typeID;
			$r = $db->Execute($sql);
			if($r->RecordCount() > 0 ) {
				deleteElementAndChildren($r->fields['id']);
			}

			$sql = "UPDATE element_attributes SET prop_val='' WHERE prop_name='checked' AND parentID=".$this->id;

		}

		if(isset($sql)) {
			$db->Execute($sql);
		}
	}

	protected function getEditorTypeID($name) {
		switch($name) {
			case 'element_editor':
				$typeID=12; break;
			case 'draggable_element_control':
				$typeID=10; break;
			case 'new_element_adder':
				$typeID=17; break;
		}
		return $typeID;
	}

}

