<?php

/**************************************************************************
 * Penguin Forms                                                          *
 * Copyright (C) 2019-2021 Mark Williams <markw@posteo.net>               *
 * Copyright (C) 2019-2021 Lee Slater <lslater@posteo.net>                *
 * Copyright (C) 2019-2021 Joe Leach <joeldn@protonmail.com>              *
 *                                                                        *
 * This file is part of Penguin Forms.                                    *
 *                                                                        *
 * Penguin Forms is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by   *
 * the Free Software Foundation, either version 3 of the License, or      *
 * (at your option) any later version.                                    *
 *                                                                        *
 * Penguin Forms is distributed in the hope that it will be useful, but   *
 * WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU       *
 * General Public License for more details.                               *
 *                                                                        *
 * You should have received a copy of the GNU General Public licenses     *
 * along with Penguin Forms. If not, see <https://www.gnu.org/licenses/>. *
 **************************************************************************/

require_once(__DIR__."/../interfaces/PenguinFormInterface.php");
require_once(__DIR__."/abstract/CombinedFormElement.php");

class FieldsetElement extends CombinedFormElement implements PenguinFormInterface {
	
	public function generateHTML() {
		if($this->editMode) {
			$html = $this->generateBackendHTML();
		} else {
			$id = 'q'.$this->id;
			$html = "<fieldset id='".$id."'>";
			$html .= "<legend>".$this->label."</legend>";
			foreach ($this->childElements as $el) {
				$html .= $el->generateHTML()."<br />";
			}
			$html .= "</fieldset>";
		}
		return $html;
	}

	public function saveToDB() {

		global $db;

		if($this->id==0) {
			// Must be a new element, so save
			$this->typeID = 8; // TODO Create a constants file!
			$sql = "INSERT INTO `form_elements` (parentID,typeID,display_order) VALUES ($this->parentID,$this->typeID,$this->display_order)";
			$db->Execute($sql);
			$this->id = $db->insert_Id();
			$this->saveTitle();
		}
		
		foreach ($this->childElements as $el) {
			$el->saveToDB();
		}
		
	}

	public function setFrontendElementIDsOfChildElements($frontendElementID) {
		foreach ($this->childElements as $el) {
			$el->setFrontendElementID($frontendElementID);
		}
	}

}
