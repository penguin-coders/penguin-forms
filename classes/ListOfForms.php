<?php

/**************************************************************************
 * Penguin Forms                                                          *
 * Copyright (C) 2019-2021 Mark Williams <markw@posteo.net>               *
 * Copyright (C) 2019-2021 Lee Slater <lslater@posteo.net>                *
 * Copyright (C) 2019-2021 Joe Leach <joeldn@protonmail.com>              *
 *                                                                        *
 * This file is part of Penguin Forms.                                    *
 *                                                                        *
 * Penguin Forms is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by   *
 * the Free Software Foundation, either version 3 of the License, or      *
 * (at your option) any later version.                                    *
 *                                                                        *
 * Penguin Forms is distributed in the hope that it will be useful, but   *
 * WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU       *
 * General Public License for more details.                               *
 *                                                                        *
 * You should have received a copy of the GNU General Public licenses     *
 * along with Penguin Forms. If not, see <https://www.gnu.org/licenses/>. *
 **************************************************************************/

require_once(__DIR__."/../interfaces/PenguinFormInterface.php");
require_once(__DIR__."/abstract/CombinedFormElement.php");

class ListOfForms extends CombinedFormElement implements PenguinFormInterface {

	protected $title = "";

	public function generateHTML() {

		$html = "<h1>Penguin forms</h1>";
		$html .= "<ul>";
		foreach ($this->childElements as $el) {
			$title = $el->getTitle();
			$formID = $el->getID();
			$html .= "<li><a href='penguin.php?formID=".$formID."'>".$title."</a></li>";
	}
		$html .= "</ul>";

		return $html;
	}

	public function loadFromDB() {
		$this->loadChildElementsFromDB();
	}

	public function saveToDB() {
		//foreach ($this->childElements as $el) {
		//	$el->saveToDB();
		//}

	}

}
