<?php

/**************************************************************************
 * Penguin Forms                                                          *
 * Copyright (C) 2019-2021 Mark Williams <markw@posteo.net>               *
 * Copyright (C) 2019-2021 Lee Slater <lslater@posteo.net>                *
 * Copyright (C) 2019-2021 Joe Leach <joeldn@protonmail.com>              *
 *                                                                        *
 * This file is part of Penguin Forms.                                    *
 *                                                                        *
 * Penguin Forms is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by   *
 * the Free Software Foundation, either version 3 of the License, or      *
 * (at your option) any later version.                                    *
 *                                                                        *
 * Penguin Forms is distributed in the hope that it will be useful, but   *
 * WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU       *
 * General Public License for more details.                               *
 *                                                                        *
 * You should have received a copy of the GNU General Public licenses     *
 * along with Penguin Forms. If not, see <https://www.gnu.org/licenses/>. *
 **************************************************************************/

require_once(__DIR__."/../interfaces/PenguinFormInterface.php");
require_once(__DIR__."/abstract/CombinedFormElement.php");

class SelectElement extends CombinedFormElement implements PenguinFormInterface {
	
	public function generateHTML() {
		if($this->editMode) {
		  $html = $this->generateBackendHTML();	
		} else {
			$id = 'q'.$this->id;
			$nameAttribute = $this->getNameAttribute();
			$html = "<label for='".$id."'>".$this->label."</label>";
			$html .= "<select name='".$nameAttribute."' id='".$id."'>";
			$html .= "<option value=''>Please select...</option>";
			foreach ($this->childElements as $el) {
				$html .= $el->generateHTML();
			}
			$html .= "</select>";
		}
		return $html;
	}

	public function generateQuestionSelectorOptions($selectedValue) {
		$selected = ($selectedValue==$this->id) ? "selected='selected'" : "";
		return "<option value='".$this->id."' ".$selected.">".$this->label."</option>";
	}

	public function saveToDB() {
		if($this->isBackendElement()) {
			$this->saveElementAttributes();
		} elseif($this->hasEditor()) {
			$this->editor->saveToDB();
			foreach($this->childElements as $el) {
				if($el->hasEditor()) {
					$el->editor->saveToDB();
				}
			}
			if($this->hasNewElementAdder()) {
				$this->newElementAdder->saveToDB();
			}
		} else {
			$this->saveSubmissionDetails();
		}
	}

	protected function loadSubmittedValue($submissionID) {

		global $db;

		$sql = "SELECT `answer` FROM `submission_details` WHERE questionID=".intval($this->id)." AND submissionID=".intval($submissionID)." ORDER BY id DESC";
		$r = $db->Execute($sql);
		if($r->RecordCount()>0) {
			$value = $r->fields['answer'];
			foreach($this->childElements as $el) {
				if($el->value==$value) {
					$el->setSelected(true);
				}
			}
		}
	}
}
