<?php

/**************************************************************************
 * Penguin Forms                                                          *
 * Copyright (C) 2019-2021 Mark Williams <markw@posteo.net>               *
 * Copyright (C) 2019-2021 Lee Slater <lslater@posteo.net>                *
 * Copyright (C) 2019-2021 Joe Leach <joeldn@protonmail.com>              *
 *                                                                        *
 * This file is part of Penguin Forms.                                    *
 *                                                                        *
 * Penguin Forms is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by   *
 * the Free Software Foundation, either version 3 of the License, or      *
 * (at your option) any later version.                                    *
 *                                                                        *
 * Penguin Forms is distributed in the hope that it will be useful, but   *
 * WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU       *
 * General Public License for more details.                               *
 *                                                                        *
 * You should have received a copy of the GNU General Public licenses     *
 * along with Penguin Forms. If not, see <https://www.gnu.org/licenses/>. *
 **************************************************************************/

require_once(__DIR__."/../interfaces/PenguinFormInterface.php");
require_once(__DIR__."/abstract/CombinedFormElement.php");

class FormPage extends CombinedFormElement implements PenguinFormInterface {

	public function generateHTML() {
		if($this->editMode) {
			$html = $this->generateBackendHTML();
		} else {
			$html = '';
			foreach ($this->childElements as $el) {
				$html .= '<p>'.$el->generateHTML().'</p>';
			}	
			$html .= '';
		}
		return $html;
	}

	public function saveToDB() {

		global $db;

		if($this->id==0) {

			$classId = get_class($this)=='FormPage' ? 2 : 14;

			$sql = "INSERT INTO `form_elements` "
				."(parentID,typeID,display_order) "
				."VALUES (".intval($this->getParentID()).",$classId,1)";

			$db->Execute($sql);
			$this->id = $db->insert_Id();

			$sql = "INSERT INTO `element_attributes` "
				."(parentID,prop_name,prop_val) "
				."VALUES (".intval($this->id).",'title',".$db->qstr($this->title).")";

			$db->Execute($sql);

		} else {
			if($this->hasEditor()) {
				$this->editor->saveToDB();
			}
			foreach ($this->childElements as $el) {
				$el->saveToDB();
			}
			if($this->hasNewElementAdder()) {
				$this->newElementAdder->saveToDB();
			}
		}
		
	}
	
	// Make sure the submit button comes last
	public function reorderSubmitButtons() {

		global $db;

		$sql = "SELECT MAX(display_order) AS do_max FROM form_elements "
			."WHERE parentID=".intval($this->id)." "
			."AND typeID !=7";

		$r = $db->Execute($sql);

		$displayOrderMax = $r->fields['do_max'];

		foreach($this->childElements as $el) {
			if($el->typeID == 7) {
				if($el->getDisplayOrder() <= $displayOrderMax) {
					$el->incrementDisplayOrder();
					$el->saveDisplayOrder();
					$displayOrderMax++;
				}
				
			} 
		}
	}

}
