<?php

/**************************************************************************
 * Penguin Forms                                                          *
 * Copyright (C) 2019-2021 Mark Williams <markw@posteo.net>               *
 * Copyright (C) 2019-2021 Lee Slater <lslater@posteo.net>                *
 * Copyright (C) 2019-2021 Joe Leach <joeldn@protonmail.com>              *
 *                                                                        *
 * This file is part of Penguin Forms.                                    *
 *                                                                        *
 * Penguin Forms is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by   *
 * the Free Software Foundation, either version 3 of the License, or      *
 * (at your option) any later version.                                    *
 *                                                                        *
 * Penguin Forms is distributed in the hope that it will be useful, but   *
 * WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU       *
 * General Public License for more details.                               *
 *                                                                        *
 * You should have received a copy of the GNU General Public licenses     *
 * along with Penguin Forms. If not, see <https://www.gnu.org/licenses/>. *
 **************************************************************************/

require_once(__DIR__."/../interfaces/PenguinFormInterface.php");
require_once(__DIR__."/abstract/CombinedFormElement.php");

class DraggableElementControl extends CombinedFormElement implements PenguinFormInterface {
	
	public function generateHTML() {

		global $cfg;

		if($this->editMode) {
			$html = $this->generateBackendHTML();
		} else {
			$id = 'q'.$this->id;
			$html = "<fieldset id='".$id."'>";

			// Edit and delete links
			$delete_href = $cfg['penguin_root']."/admin/delete.php?delete=".$this->frontendElementID."&redirect=".urlencode($_SERVER['REQUEST_URI']);
			$html .= "<span class='penguin-delete-question'>";
			$html .= "<a class='element-edit' href='?elementID=".$this->frontendElementID."'>Edit</a>&nbsp;";
			$html .= "<a class='element-delete' href='".$delete_href."'>Delete</a>";
			$html .= "</span>";

			foreach ($this->childElements as $el) {
				$html .= $el->generateHTML()."<br />";
			}

			$html .= "</fieldset>";
		}

		return $html;
	}

	public function saveToDB() {

		global $db;

		if($this->hasEditor()) {
			$this->editor->saveToDB();
			foreach($this->childElements as $el) {
				if($el->hasEditor()) {
					$el->editor->saveToDB();
				}
			}
			if($this->hasNewElementAdder()) {
				$this->newElementAdder->saveToDB();
			}
		} else {
			if($this->id==0) {
				// Must be a new element, so save
				$this->typeID = 12; // TODO Create a constants file!
				$sql = "INSERT INTO `form_elements` (parentID,typeID,display_order) VALUES ($this->parentID,$this->typeID,$this->display_order)";
				$db->Execute($sql);
				$this->id = $db->insert_Id();
				$this->saveTitle();
			}
			
			foreach ($this->childElements as $el) {
				$el->saveToDB();
			}
		}
		
	}

	public function setFrontendElementIDsOfChildElements($frontendElementID) {
		foreach ($this->childElements as $el) {
			$el->setFrontendElementID($frontendElementID);
		}
	}

}
