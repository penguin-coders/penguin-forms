<?php

/**************************************************************************
 * Penguin Forms                                                          *
 * Copyright (C) 2019-2021 Mark Williams <markw@posteo.net>               *
 * Copyright (C) 2019-2021 Lee Slater <lslater@posteo.net>                *
 * Copyright (C) 2019-2021 Joe Leach <joeldn@protonmail.com>              *
 *                                                                        *
 * This file is part of Penguin Forms.                                    *
 *                                                                        *
 * Penguin Forms is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by   *
 * the Free Software Foundation, either version 3 of the License, or      *
 * (at your option) any later version.                                    *
 *                                                                        *
 * Penguin Forms is distributed in the hope that it will be useful, but   *
 * WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU       *
 * General Public License for more details.                               *
 *                                                                        *
 * You should have received a copy of the GNU General Public licenses     *
 * along with Penguin Forms. If not, see <https://www.gnu.org/licenses/>. *
 **************************************************************************/

abstract class SimpleFormElement {

	protected $id; // database ID - element ID will be "q".$id
	protected $display_order;
	protected $label;	
	protected $name;
	protected $value;
	protected $help;
	protected $parentID;
	protected $editor; // for backend elements
	protected $frontendElementID; // for elements within a backend fieldset
	protected $editMode; 
	protected $formID;

	public function __construct($id = 0, $typeID = 0, $displayOrder = 1, $label = '', $value = '', $name = '') {

		$this->id = $id;
		$this->typeID = $typeID;
		$this->display_order = $displayOrder;
		$this->label = $label;
		$this->name = $name;
		$this->value = $value;
		$this->editor = '';
		$this->frontendElementID = 0;
		$this->parentID = 0;
		$this->editMode = false;
		$thid->formID = 0;
	}

	public function generateQuestionSelectorOptions($selectedValue) {
		$selected = ($selectedValue==$this->id) ? "selected='selected'" : "";
		return "<option value='".$this->id."' ".$selected.">".$this->label."</option>";
	}

	public function getTitle() {
		if(isset($this->title)) {
			return $this->title;
		} else {
			return $this->label;
		}
	}

	public function getID() {
		return $this->id;
	}

	public function setEditMode($mode = true) {
		$this->editMode = $mode;
	}

	public function setFormID($formID) {
		$this->formID = $formID;
	}
	
	public function setParentID($parentID) {
		$this->parentID = $parentID;
	}

	public function getParentID() {
		return $this->parentID;
	}

	public function getTypeID() {
		return $this->typeID;
	}

	public function setValue($val) {
		$this->value = $val;
	}
	
	public function setFrontendElementID($id) {
		$this->frontendElementID = intval($id);
	}

	public function setDisplayOrder($displayOrder) {
		$this->display_order = $displayOrder;
	}

	public function getDisplayOrder() {
		return $this->display_order;
	}

	public function incrementDisplayOrder() {
		$this->display_order++;
	}

	protected function saveDisplayOrder() {

		global $db;

		$sql = "UPDATE `form_elements` SET `display_order`=".intval($this->display_order)." "
			."WHERE `id`=".intval($this->id);
		$db->Execute($sql);
	}

	protected function isBackendElement() {
		return ($this->frontendElementID > 0);
	}

	protected function hasEditor() {
		return ($this->editor != '');
	}

	protected function getNameAttribute() {
		if($this->isBackendElement()) {
			return $this->name."[".$this->frontendElementID."]";
		} else {
			return $this->name;
		}
	}

	public function loadFromDB() {
		$this->loadElementAttributesFromDB();
	}

	public function saveToDB() {
		if($this->isBackendElement()) {
			$this->saveElementAttributes();
		} elseif($this->hasEditor()) {
			$this->editor->saveToDB();
		} else {
			$this->saveSubmissionDetails();
		}
	}

	protected function loadElementAttributesFromDB() {

		// Get element attributes
		global $db;
		$sql = "SELECT `prop_name`,`prop_val` FROM `element_attributes` WHERE parentID=".intval($this->id);

		$r = $db->Execute($sql);
		for ($i=0; $i<$r->RecordCount(); $i++) {
			$name = $r->fields['prop_name'];
			$this->$name = $r->fields['prop_val'];
			$r->MoveNext();
		}

	}	

	protected function loadSubmittedValue($submissionID) {

		global $db;

		$sql = "SELECT `answer` FROM `submission_details` WHERE questionID=".intval($this->id)." AND submissionID=".intval($submissionID)." ORDER BY id DESC";
		$r = $db->Execute($sql);
		if($r->RecordCount()>0) {
			$this->value = $r->fields['answer'];
		}
	}

	public function loadEditors() {
		$this->loadEditorFromDB();
	}

	protected function loadEditorFromDB($fieldsetType = '') {

		global $db;

		switch($fieldsetType) {
			case 'draggable': 
				$selectField = 'draggable_element_control'; 
				$fieldsetProperty = 'editor';
				break;
			case 'newelement': 
				$selectField = 'new_element_adder'; 
				$fieldsetProperty = 'newElementAdder';
				break;
			default: 
				$selectField = 'element_editor';
				$fieldsetProperty = 'editor';
		}

		$sql = "SELECT ".$selectField." FROM form_element_types "
			."INNER JOIN `form_elements` "
			."ON `form_elements`.`typeID` = `form_element_types`.`id` "
			."WHERE `form_elements`.`id`=".intval($this->id);
		
		$r = $db->Execute($sql);
			
		$bfs = $r->fields[$selectField];
		if(is_numeric($bfs)) {

			$className = getElementType($bfs);

			$el = new $className($bfs);
			$el->setFormID($this->formID);
			$el->loadFromDB();
			$el->setFrontendElementID($this->id);

			foreach ($el->childElements as $sfe) {
				$prop = strtolower($sfe->name);
				$sfe->setValue($this->$prop);
				$sfe->setFrontendElementID($this->id);
				$el->setFormID($this->formID);
			}

			$this->$fieldsetProperty = $el;
		} 				
			
	}

	protected function getSubmissionDetailsID($submissionID) {

		global $db;

		$sql = "SELECT id FROM `submission_details` WHERE questionID=".intval($this->id)." AND submissionID=".intval($submissionID);
		$r = $db->Execute($sql);
		
		if($r->RecordCount()>0) {
			return $r->fields['id'];
		} else {
			return -1;
		}
	}

	protected function saveSubmissionDetails() {

		global $db, $_POST;

		if(isset($_POST[$this->name])) {

			$submissionID = getSubmissionID();
			$submissionDetailsID = $this->getSubmissionDetailsID($submissionID);
			$answer = $_POST[$this->name];

			if($submissionDetailsID>0) {
				$sql = "UPDATE `submission_details` SET answer=".$db->qstr($answer).", tainted=0 WHERE id=".intval($submissionDetailsID);
			} else {
				$sql = "INSERT INTO `submission_details` (submissionID,questionID,answer,tainted) "
					."VALUES (".$submissionID.",".$this->id.",".$db->qstr($answer).",0)";
			}

			$db->Execute($sql);

		}
	}

	protected function taintAns($submissionID) {

		global $db;

		$sql = "UPDATE `submission_details` SET tainted=1 WHERE id=".intval($this->getSubmissionDetailsID($submissionID));
		$db->Execute($sql);
	}

	public function saveElementAttributes() {
		global $db, $_POST;
		if(isset($_POST[$this->name][$this->frontendElementID])) {

			$answer = $_POST[$this->name][$this->frontendElementID];

			switch($this->name) {
				case 'new_form_title':
					$form = new PenguinForm();
					$form->setParentID($this->frontendElementID);
					$form->setTitle($answer);
					$form->saveToDB();
					break;
				case 'new_page':
					if($answer!='') {
						$page = new $answer();
						print_r($page);
						$page->setParentID($this->frontendElementID);
						//$page->setTitle($answer);
						$page->saveToDB();
					}
					break;
				case 'new_editor':
				case 'new_element_type':
					if($answer != "") {
						$sql = "INSERT INTO `form_elements` (parentID,typeID,display_order) VALUES ($this->frontendElementID,".intval($answer).",$this->display_order)";
					}
					break;
				case 'new_backend_element':
					if($answer != "") {

						// Create configurator as child of "list of forms"
						$configurator = new ElementConfigurator();
						$configurator->setParentID($this->frontendElementID);
						$configurator->setTitle($answer);
						$configurator->createClassFile();
						$configurator->saveToDB();
						$configurator->setFormID($this->formID);

						// Associate configurator with it's class
						$sql = "INSERT INTO form_element_types (class_name, configurator) VALUES (".$db->qstr($answer).",".$configurator->id.")";

					}
					break;
				case 'display_order':
					$sql = "UPDATE `form_elements` SET `display_order`=".intval($answer)." "
						."WHERE `id`=".intval($this->frontendElementID);
					break;
				default:

					$sql = "SELECT `id` FROM `element_attributes` "
						."WHERE `parentID`=".intval($this->frontendElementID)." "
						."AND `prop_name`=".$db->qstr($this->name);

					$r = $db->Execute($sql);

					if($r->RecordCount() > 0) {
						$sql = "UPDATE `element_attributes` SET `prop_val`=".$db->qstr($answer)." "
							."WHERE `id`=".intval($r->fields['id']);
					} else {
						$sql = "INSERT INTO `element_attributes` (parentID,prop_name,prop_val) "
							."VALUES (".intval($this->frontendElementID)
							.",".$db->qstr($this->name)
							.",".$db->qstr($answer).")";
					}
			}

			if(isset($sql)) {
				$db->Execute($sql);
			}

		}
	}

	public function generateJSON($cascade = false) {
		global $_SESSION;
		global $db;
		$json = [];
		$json["class"] = get_class($this);
		$sql = "select * from element_attributes where parentID=?";
		$r = $db->Execute($sql,[$this->id]);
		for ($i=0; $i<$r->RecordCount(); $i++) {
			$json["attributes"][$r->fields["prop_name"]] = $r->fields["prop_val"];
			$r->MoveNext();
		}

		return $json;
	}

	public function generateBackendHTML() {
		if(is_object($this->editor)) {
			$html = $this->editor->generateHTML();
		} else {
			$html = "<p />CombinedFormElement: Missing editor for element ID=".intval($this->id);
		}
		return $html;
	}

	protected function isDisplayable() {
		return true;
	}

}

