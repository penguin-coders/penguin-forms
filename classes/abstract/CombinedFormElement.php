<?php

/**************************************************************************
 * Penguin Forms                                                          *
 * Copyright (C) 2019-2021 Mark Williams <markw@posteo.net>               *
 * Copyright (C) 2019-2021 Lee Slater <lslater@posteo.net>                *
 * Copyright (C) 2019-2021 Joe Leach <joeldn@protonmail.com>              *
 *                                                                        *
 * This file is part of Penguin Forms.                                    *
 *                                                                        *
 * Penguin Forms is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by   *
 * the Free Software Foundation, either version 3 of the License, or      *
 * (at your option) any later version.                                    *
 *                                                                        *
 * Penguin Forms is distributed in the hope that it will be useful, but   *
 * WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU       *
 * General Public License for more details.                               *
 *                                                                        *
 * You should have received a copy of the GNU General Public licenses     *
 * along with Penguin Forms. If not, see <https://www.gnu.org/licenses/>. *
 **************************************************************************/

require_once(__DIR__."/SimpleFormElement.php");

abstract class CombinedFormElement extends SimpleFormElement {

	protected $childElements = array();
	protected $title = "";
	protected $newElementAdder; // Fieldset for adding new elements in edit mode

	public function setTitle($title) {
		$this->title = $title;
	}
	
	public function getTitle() {
		return $this->title;
	}

	protected function saveTitle() {

		global $db; 

		$sql = "INSERT INTO `element_attributes` "
				."(parentID,prop_name,prop_val) "
				."VALUES (".intval($this->id).",".$db->qstr('title').",".$db->qstr($this->title).")";

		$db->Execute($sql);
	}

	protected function addElement($className, $elementID, $typeID, $displayOrder = 1) {

		$el = new $className($elementID, $typeID, $displayOrder);
		$el->setFormID($this->formID);
		$el->loadFromDB();
		$el->setParentID($this->id);
		$this->childElements[] = $el;
	}

	public function generateQuestionSelectorOptions($selectedValue) {
		$html = "";
		foreach($this->childElements as $el) {
			$html .= $el->generateQuestionSelectorOptions($selectedValue);
		}
		return $html;
	}

	public function getNextDisplayOrder() {
		// The display_order of the next element will need to be 1 more than 
		// the number of elements counted, so set $c to 1.
		$c = 1; 
		foreach($this->childElements as $el) {
			if($el->typeID < 7) {
				$c++;
			}
		}
		return $c;	
	}
	
	protected function taintAns($submissionID = -1) {

		if($submissionID<0) {
			$submissionID = getSubmissionID();
		}

		foreach ($this->childElements as $el) {
			$el->taintAns($submissionID);
		}
		
	}

	public function loadEditors() {
		$this->loadEditorFromDB();
		foreach ($this->childElements as $el) {
			$el->loadEditorFromDB("draggable");
		}
		$this->loadEditorFromDB("newelement");
	}

	public function generateJSON($cascade = false) {
		global $_SESSION;
		global $db;
		$json = [];
		$json["class"] = get_class($this);
		$json["attributes"] = [];
		$sql = "select * from element_attributes where parentID=?";
		$r = $db->Execute($sql,[$this->id]);
		for ($i=0; $i<$r->RecordCount(); $i++) {
			$json["attributes"][$r->fields["prop_name"]] = $r->fields["prop_val"];
			$r->MoveNext();
		}

		foreach ($this->childElements as $index => $el) {
			$json["childElements"][$index] = $el->generateJSON();
		}

		return $json;
	}

	public function generateBackendHTML() {

		if(is_object($this->editor)) {
				$html = $this->editor->generateHTML();
			} else {
				$html = "<p />CombinedFormElement: Missing editor for element ID=".intval($this->id);
				Kint::trace();
			}

		$html .= "<div id='draggableFieldsets'><div>";

		foreach ($this->childElements as $el) {
			if(is_object($el->editor)) {
				$html .= $el->editor->generateHTML();
			} else {
				$html .= "<p />CombinedFormElement: Missing editor for child element ID=".intval($el->id);
				Kint::trace();
			}
		}

		$html .= "</div></div><br class='clearfloat' />";

		if($this->hasNewElementAdder()) {
			$html .= $this->newElementAdder->generateHTML();
		}

		return $html;
	}

	protected function hasNewElementAdder() {
		return ($this->newElementAdder != '');
	}

	protected function loadSubmittedValue($submissionID) {
		foreach($this->childElements as $el) {
			$el->loadSubmittedValue($submissionID);
		}
	}

	public function loadFromDB() {
		$this->loadElementAttributesFromDB();
		$this->loadChildElementsFromDB();
	}

	protected function loadChildElementsFromDB($classnameSuffix = '') {

		// Get form elements
		global $db;
		$sql = "SELECT `form_elements`.`id`, `form_elements`.`typeID`, "
			."`form_element_types`.`class_name`, "
			."`form_elements`.`display_order` "
			."FROM `form_elements` "
			."INNER JOIN `form_element_types` "
			."ON `form_elements`.`typeID` = `form_element_types`.`id` "
			."WHERE `parentID`=".intval($this->id)." "
			."ORDER BY `form_elements`.`display_order`";

		$r = $db->Execute($sql);
		for ($i=0; $i<$r->RecordCount(); $i++) {
			$this->addElement($r->fields['class_name'].$classnameSuffix, $r->fields['id'], $r->fields['typeID'], $r->fields['display_order']);
			$r->MoveNext();
		}

	}

	public function attachElement($el) {
		$this->childElements[] = $el;
	}

}
