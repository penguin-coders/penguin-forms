<?php

/**************************************************************************
 * Penguin Forms                                                          *
 * Copyright (C) 2019-2021 Mark Williams <markw@posteo.net>               *
 * Copyright (C) 2019-2021 Lee Slater <lslater@posteo.net>                *
 * Copyright (C) 2019-2021 Joe Leach <joeldn@protonmail.com>              *
 *                                                                        *
 * This file is part of Penguin Forms.                                    *
 *                                                                        *
 * Penguin Forms is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by   *
 * the Free Software Foundation, either version 3 of the License, or      *
 * (at your option) any later version.                                    *
 *                                                                        *
 * Penguin Forms is distributed in the hope that it will be useful, but   *
 * WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU       *
 * General Public License for more details.                               *
 *                                                                        *
 * You should have received a copy of the GNU General Public licenses     *
 * along with Penguin Forms. If not, see <https://www.gnu.org/licenses/>. *
 **************************************************************************/

require_once(__DIR__."/../interfaces/PenguinFormInterface.php");
require_once(__DIR__."/abstract/CombinedFormElement.php");

class PenguinForm extends CombinedFormElement implements PenguinFormInterface {

	protected $currentPage = -1;
	protected $submission_email = '';

	public function generateHTML() {
		if($this->editMode) {
			$html = $this->generateBackendHTML();
		} else {

			$html = "<h1>".$this->title."</h1>";
			global $_SESSION;
			$html .= "<form method='post'>";
			if($this->currentPage==-1) {
				foreach ($this->childElements as $el) {
					$html .= $el->generateHTML();
				}
			} else {
				$html .= $this->childElements[$this->currentPage]->generateHTML();
			}
			$html .= "</form>";
		}

		return $html;
	}

	public function rmTaintedAnswers() {
		
		global $db;

		$submissionID = getSubmissionID();

		$sql = "DELETE FROM submission_details WHERE tainted=1 AND submissionID=".$submissionID;
		$db->Execute($sql);
	}

	public function generateAnswersTable($submissionID) {

		global $db;

		$sql = "SELECT * FROM submission_details WHERE submissionID=".$submissionID;
		$r=$db->Execute($sql);

		$html = "";
		$ans = [];

		if($r->RecordCount() >0) {

			for ($i=0; $i<$r->RecordCount(); $i++) {
				$answers[$r->fields['questionID']] = $r->fields['answer'];
				$r->MoveNext();
			}

			$html = "<table>\n";
			$html .= "<tr><th>Question</th><th>Answer</th></tr>\n";
		
			foreach($this->getQuestions() as $page) {
				foreach($page as $questionID => $question) {
					$html .= "<tr><td>".$question."</td><td>".$answers[$questionID]."</td></tr>\n";
				}
			}

			$html .= "</table>";

		}

		return $html;
	}

	public function mailForm() {
		if(isset($this->submission_email) && $this->submission_email != '') {
			$body = $this->generateAnswersTable($_SESSION['submissionID']);

			$sender="markw@posteo.net";
			$subject="Penguin forms submission";
					  
			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";

			mail($this->submission_email, $subject, $message, $headers); 
		}
	}

	public function setCurrentPage() {
		if(isset($_SESSION['currentPage'])) {
			$this->currentPage = $_SESSION['currentPage'];
		} else {
			$this->currentPage = 0;
			$_SESSION['currentPage'] = $this->currentPage;
		}
	}

	public function changePage($dir) {
		$prev = ["Prev","Previous","Back"];
		if(in_array($dir,$prev)) {
			$this->prevPage();
		} elseif (intval($dir)) {
			// Load specific page
			$this->gotoPage($dir);
		} else {
			$this->nextPage();
		}
	}

	public function nextPage() {
		global $_SESSION;
		$this->currentPage++;
		$_SESSION['currentPage'] = $this->currentPage;
		if(!$this->endOfForm()) {
			// Check page conditions
			if(!$this->childElements[$this->currentPage]->isDisplayable()) {
				$this->nextPage();
			}

			if(isset($_SESSION['submissionID'])) {
				$this->childElements[$this->currentPage]->loadSubmittedValue($_SESSION['submissionID']);
			}
		}
	}

	public function prevPage() {
		global $_SESSION;
		if($this->currentPage>0) {
			$this->currentPage--;
			$_SESSION['currentPage'] = $this->currentPage;
			
			// Check page conditions
			if(!$this->childElements[$this->currentPage]->isDisplayable()) {
				$this->prevPage();
			}
			
			if(isset($_SESSION['submissionID'])) {
				$this->childElements[$this->currentPage]->loadSubmittedValue($_SESSION['submissionID']);
			}
		}
	}

	public function gotoPage($pageNum) {
		global $_SESSION;
		$pageNum--;
		if (!$this->childElements[$pageNum]->isDisplayable()) {
			// If the requested page is not displayable, do not goto
			return false;
		}
		$this->currentPage = $pageNum;
		$_SESSION['currentPage'] = $this->currentPage;
		if (isset($_SESSION['submissionID'])) {
			$this->childElements[$this->currentPage]->loadSubmittedValue($_SESSION['submissionID']);
		}
	}

	public function endOfForm() {
		return ($this->currentPage==count($this->childElements));
	}

	public function makeBackendForm() {

		$this->addElement('fieldsetElement',16,8); // orphan fieldset for adding a new element

		$nextDisplayOrder = $this->childElements[0]->getNextDisplayOrder();
		$pageID = $this->childElements[0]->getID();

		end($this->childElements);
		$i = key($this->childElements); 
		$this->childElements[$i]->childElements[0]->setFrontendElementID($pageID);
		$this->childElements[$i]->childElements[0]->setDisplayOrder($nextDisplayOrder);
		$this->addSubmit();
	}

	public function addSubmit() {
		$this->addElement('SubmitButton',14,7); // orphan submit button
	}


	public function saveToDB() {

		global $db;

		if(isset($_POST['new_form_title'])) {
			$sql = "INSERT INTO `form_elements` "
				."(parentID,typeID,display_order) "
				."VALUES (".intval($this->getParentID()).",1,1)";

			$db->Execute($sql);
			$this->id = $db->insert_Id();
			$this->saveTitle();
		} elseif($this->hasEditor()) {

			$this->editor->saveToDB();
			foreach ($this->childElements as $el) {
				$el->saveToDB();
			}
			if($this->hasNewElementAdder()) {
				$this->newElementAdder->saveToDB();
			}

		} else {
			if($this->id==0) { // i.e. if the form is a wrapper for editing an element
				foreach ($this->childElements as $el) {
					$el->saveToDB();
				}
			} else { // i.e. it must be a fron-end form
				$this->childElements[$this->currentPage]->saveToDB();
			}
			
		}
	}

	public function getQuestions() {
		$qs = [];

		foreach ($this->childElements as $page) {
			foreach ($page->childElements as $el) {
				if($el->typeID != 7) {
					$qs[$page->title][$el->id] = $el->label;
				}
			}
		}

		return $qs;
	}

	public function setEditModeForPages() {
		foreach($this->childElements as $page) {
			$page->loadEditors();
			$page->setEditMode();
		}
	}

}

