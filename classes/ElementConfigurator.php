<?php

/**************************************************************************
 * Penguin Forms                                                          *
 * Copyright (C) 2019-2021 Mark Williams <markw@posteo.net>               *
 * Copyright (C) 2019-2021 Lee Slater <lslater@posteo.net>                *
 * Copyright (C) 2019-2021 Joe Leach <joeldn@protonmail.com>              *
 *                                                                        *
 * This file is part of Penguin Forms.                                    *
 *                                                                        *
 * Penguin Forms is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by   *
 * the Free Software Foundation, either version 3 of the License, or      *
 * (at your option) any later version.                                    *
 *                                                                        *
 * Penguin Forms is distributed in the hope that it will be useful, but   *
 * WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU       *
 * General Public License for more details.                               *
 *                                                                        *
 * You should have received a copy of the GNU General Public licenses     *
 * along with Penguin Forms. If not, see <https://www.gnu.org/licenses/>. *
 **************************************************************************/

require_once(__DIR__."/../interfaces/PenguinFormInterface.php");
require_once(__DIR__."/abstract/CombinedFormElement.php");

class ElementConfigurator extends CombinedFormElement implements PenguinFormInterface {
	
	public function generateHTML() {
		if($this->editMode) {
			$html = $this->generateBackendHTML();
		} else {
			$id = 'q'.$this->id;
			$html = "<fieldset id='".$id."'>";
			$html .= "<legend>".$this->label."</legend>";
			foreach ($this->childElements as $el) {
				$html .= $el->generateHTML();
			}
			$html .= "</fieldset>";
		}
		return $html;
	}
	
	public function loadEditors() {
		parent::loadEditors();
		// Correct editor checkboxes
		$enabled_editors = [];
		foreach($this->childElements as $el) {
			$enabled_editors[] = $el->typeID;
		}
		foreach($this->newElementAdder->childElements as $el) {
			if(in_array($el->value, $enabled_editors)) {
				$el->setChecked(true);
			}
		}
	}

	protected function loadEditorFromDB($fieldsetType = '') {

		global $db;

		switch($fieldsetType) {
			case 'draggable': 
				$selectField = 'draggable_element_control'; 
				$fieldsetProperty = 'editor';
				break;
			case 'newelement': 
				$selectField = 'new_element_adder'; 
				$fieldsetProperty = 'newElementAdder';
				break;
			default: 
				$selectField = 'element_editor';
				$fieldsetProperty = 'editor';
		}

		$sql = "SELECT ".$selectField." FROM form_element_types "
			."INNER JOIN `form_elements` "
			."ON `form_elements`.`typeID` = `form_element_types`.`id` "
			."WHERE `form_elements`.`id`=".intval($this->id);
		
		$r = $db->Execute($sql);
			
		$bfs = $r->fields[$selectField];
		if(is_numeric($bfs)) {

			$className = getElementType($bfs);

			$el = new $className($bfs);
			$el->setFormID($this->formID);
			$el->loadFromDB();
			$el->setFrontendElementID($this->id);

			foreach ($el->childElements as $sfe) {
				$sfe->setFrontendElementID($this->id);
				$el->setFormID($this->formID);
			}

			$this->$fieldsetProperty = $el;

			// Re-load any elements which need a 2nd pass, such as ClassnameEditor 
			if($fieldsetProperty=="editor") {
				foreach($this->editor->childElements as $el) {
					$el->loadFromDB();
				}
			}
		} 				
			
	}

	public function saveToDB() {

		global $db;

		if($this->hasEditor()) {
			$this->editor->saveToDB();
			foreach($this->childElements as $el) {
				if($el->hasEditor()) {
					$el->editor->saveToDB();
				}
			}
			if($this->hasNewElementAdder()) {

				$this->newElementAdder->saveToDB();
d("Saved new elements");
				// Re-load so we can get the ids of new elements
				$this->childElements = [];
				$this->loadFromDB();
				d("Re-loaded");
				d($this->childElements);
				// Now update form_element_types table
				$props_to_update = ['element_editor' => 'NULL', 'draggable_element_control' => 'NULL', 'new_element_adder' => 'NULL'];
				foreach($this->childElements as $el) {
					switch($el->typeID) {
					case 12:
						$props_to_update['element_editor'] = $el->id; break;
					case 10:
						$props_to_update['draggable_element_control'] = $el->id; break;
					case 17:
						$props_to_update['new_element_adder'] = $el->id; break;
					}
				}

				array_walk($props_to_update, function(&$val, $key) {
					$val = "$key=$val";
				});

				$sql = "UPDATE form_element_types SET ".implode(', ',$props_to_update)." WHERE class_name=".$db->qstr($this->title);
				d($sql);
				$db->Execute($sql);
					
			}
		} else {
			if($this->id==0) {
				// Must be a new element, so save
				$this->typeID = 16; // TODO Create a constants file!
				$sql = "INSERT INTO `form_elements` (parentID,typeID,display_order) VALUES ($this->parentID,$this->typeID,$this->display_order)";
				$db->Execute($sql);
				$this->id = $db->insert_Id();
				$this->saveTitle();
			}
			
			foreach ($this->childElements as $el) {
				$el->saveToDB();
			}
		}
		
	}

	public function setFrontendElementIDsOfChildElements($frontendElementID) {
		foreach ($this->childElements as $el) {
			$el->setFrontendElementID($frontendElementID);
		}
	}

	protected function createClassFile() {

		// For now, assume it's a simple form element
		$classToExtend = "SimpleFormElement";
		$className = $this->title;

		$classFilePHP = <<<ENDPHP
<?php

/**************************************************************************
 * Penguin Forms                                                          *
 * Copyright (C) 2019-2021 Mark Williams <markw@posteo.net>               *
 * Copyright (C) 2019-2021 Lee Slater <lslater@posteo.net>                *
 * Copyright (C) 2019-2021 Joe Leach <joeldn@protonmail.com>              *
 *                                                                        *
 * This file is part of Penguin Forms.                                    *
 *                                                                        *
 * Penguin Forms is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by   *
 * the Free Software Foundation, either version 3 of the License, or      *
 * (at your option) any later version.                                    *
 *                                                                        *
 * Penguin Forms is distributed in the hope that it will be useful, but   *
 * WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU       *
 * General Public License for more details.                               *
 *                                                                        *
 * You should have received a copy of the GNU General Public licenses     *
 * along with Penguin Forms. If not, see <https://www.gnu.org/licenses/>. *
 **************************************************************************/

require_once(__DIR__."/../interfaces/PenguinFormInterface.php");
require_once(__DIR__."/abstract/$classToExtend.php");
		
class $className extends $classToExtend implements PenguinFormInterface {
	
	public function generateHTML() {
		if(DOLLARthis->editMode) {
		  DOLLARhtml = DOLLARthis->editor->generateHTML();	
		} else {
			DOLLARhtml = "<p>Output HTML has not been specified for element '$className'. Please implement a generateHTML method.</p>";
		}
		return DOLLARhtml;
	}

	// Un-comment to over-ride default method
	/*
	public function loadFromDB() {
		// Add code here
	}
	*/

	
	// Un-comment to over-ride default method
	/*
	public function saveToDB() {
		// Add code here
	}
	*/

}
ENDPHP;

		$classFilePHP = preg_replace('/DOLLAR/','$',$classFilePHP);

		file_put_contents(__DIR__."/".$className.".php", $classFilePHP, LOCK_EX);
	}
}
