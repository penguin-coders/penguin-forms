<?php

/**************************************************************************
 * Penguin Forms                                                          *
 * Copyright (C) 2019-2021 Mark Williams <markw@posteo.net>               *
 * Copyright (C) 2019-2021 Lee Slater <lslater@posteo.net>                *
 * Copyright (C) 2019-2021 Joe Leach <joeldn@protonmail.com>              *
 *                                                                        *
 * This file is part of Penguin Forms.                                    *
 *                                                                        *
 * Penguin Forms is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by   *
 * the Free Software Foundation, either version 3 of the License, or      *
 * (at your option) any later version.                                    *
 *                                                                        *
 * Penguin Forms is distributed in the hope that it will be useful, but   *
 * WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU       *
 * General Public License for more details.                               *
 *                                                                        *
 * You should have received a copy of the GNU General Public licenses     *
 * along with Penguin Forms. If not, see <https://www.gnu.org/licenses/>. *
 **************************************************************************/

session_start();

require_once($cfg['penguin_filepath']."/includes/common_functions.php");

// Connect to DB
$dbConfig = require($cfg['penguin_filepath']."/config/database.php");
if(isset($_SESSION['testdb']) && $_SESSION['testdb']) {
	$dbConfig['database'] = $dbConfig['test_database'];
}
$db = ADONewConnection($dbConfig['driver']);
$db->debug = $dbConfig['debug'];
$db->Connect($dbConfig['address'], $dbConfig['username'], $dbConfig['password'], $dbConfig['database']);
// TODO: Connection failed error handling

