<?php

/**************************************************************************
 * Penguin Forms                                                          *
 * Copyright (C) 2019-2021 Mark Williams <markw@posteo.net>               *
 * Copyright (C) 2019-2021 Lee Slater <lslater@posteo.net>                *
 * Copyright (C) 2019-2021 Joe Leach <joeldn@protonmail.com>              *
 *                                                                        *
 * This file is part of Penguin Forms.                                    *
 *                                                                        *
 * Penguin Forms is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by   *
 * the Free Software Foundation, either version 3 of the License, or      *
 * (at your option) any later version.                                    *
 *                                                                        *
 * Penguin Forms is distributed in the hope that it will be useful, but   *
 * WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU       *
 * General Public License for more details.                               *
 *                                                                        *
 * You should have received a copy of the GNU General Public licenses     *
 * along with Penguin Forms. If not, see <https://www.gnu.org/licenses/>. *
 **************************************************************************/

function getSubmissionID() {

	global $db, $_SESSION, $formID; // TODO - make code not rely on global $formID

	if(!isset($_SESSION['submissionID'])) {
		$sql = "INSERT INTO `form_submissions` (formID) VALUES (".intval($formID).")";
		$db->Execute($sql);
		$_SESSION['submissionID'] = $db->insert_Id();
	}

	return $_SESSION['submissionID'];
}

function getElementType($elementID) {

	global $db;

	$sql = "SELECT form_elements.id, form_element_types.class_name "
		."FROM form_elements "
		."INNER JOIN form_element_types "
		."ON form_elements.typeID = form_element_types.id "
		."WHERE form_elements.id=".intval($elementID);

	$r = $db->Execute($sql);

	return $r->fields['class_name'];
}

function getParentID($elementID) {
	
	global $db;

	$sql = "SELECT parentID FROM form_elements WHERE id=?";
	$r =	$db->Execute($sql, [$elementID]);

	return $r->fields['parentID'];
}

function getFormID($elementID) {
	
	global $db;

	$parentID = getParentID($elementID);
	if($parentID==0) {
		return 0;
	}
	
	$type = getElementType($parentID);
	if($type=="PenguinForm") {
		return $parentID;
	} else {
		return getFormID($parentID);
	}

}

function deleteElementAndChildren($id) {

	global $db;

	$id = intval($id);

	$db->Execute("delete from element_attributes where parentID=".$id);
	$db->Execute("delete from form_elements where id=".$id);

	$r = $db->Execute("SELECT id FROM form_elements WHERE parentID = ".$id);
	for ($i=0; $i<$r->RecordCount(); $i++) {
		deleteElementAndChildren($r->fields["id"]);
		$r->MoveNext();
	}
}
