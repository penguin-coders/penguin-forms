<?php

/**************************************************************************
 * Penguin Forms                                                          *
 * Copyright (C) 2019-2021 Mark Williams <markw@posteo.net>               *
 * Copyright (C) 2019-2021 Lee Slater <lslater@posteo.net>                *
 * Copyright (C) 2019-2021 Joe Leach <joeldn@protonmail.com>              *
 *                                                                        *
 * This file is part of Penguin Forms.                                    *
 *                                                                        *
 * Penguin Forms is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by   *
 * the Free Software Foundation, either version 3 of the License, or      *
 * (at your option) any later version.                                    *
 *                                                                        *
 * Penguin Forms is distributed in the hope that it will be useful, but   *
 * WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU       *
 * General Public License for more details.                               *
 *                                                                        *
 * You should have received a copy of the GNU General Public licenses     *
 * along with Penguin Forms. If not, see <https://www.gnu.org/licenses/>. *
 **************************************************************************/

?>

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width">
<title>Penguin forms</title>
<link rel='stylesheet' href='<?php print $cfg['penguin_root']; ?>/includes/css/penguinStyle.css' />
<?php
$jQueryConfig = require_once($cfg['penguin_filepath']."/config/jquery.php");
print "<script src='".$jQueryConfig['jQueryLocation']."'></script>";
print "<script src='".$jQueryConfig['jQueryUILocation']."'></script>";
?>
</head>
